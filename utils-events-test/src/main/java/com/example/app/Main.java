package com.example.app;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.events.EventBus;
import edu.szyrek.utils.events.EventBusID;
import edu.szyrek.utils.events.EventBusShutDownEvent;
import edu.szyrek.utils.events.EventListener;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bebebaba on 2016-07-31.
 */
@Slf4j
public class Main implements EventListener<EventBusShutDownEvent> {

    @Override
    public void handle(EventBusShutDownEvent event) {
        System.out.println("Hello from utils-events-test");
        event.setHandled(true);
    }

    public static void main(String args[]) throws IDTakenException, InterruptedException {
        Main main = new Main();
        EventBus.MAIN.registerListener(main);
    }

}
