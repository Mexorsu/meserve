package com.example.app;

import edu.szyrek.utils.annotation.ConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigTypeAdapter;
import org.json.simple.JSONObject;

/**
 * Created by bebebaba on 2016-07-31.
 */
@ConfigType(adapter = JSONConfigTypeAdapter.class)
public class ExampleJSONType implements JSONConfigType {
    public String name;
    public String surname;

    @Override
    public void fromJSON(JSONObject json) {
        this.name = (String) json.get("name");
        this.surname = (String) json.get("surname");
    }

    @Override
    public String toString() {
        return "Person: "+name+" "+surname;
    }
}
