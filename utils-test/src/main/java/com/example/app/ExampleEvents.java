package com.example.app;

import edu.szyrek.utils.events.Event;
import edu.szyrek.utils.annotations.EventListener;

/**
 * Created by bebebaba on 2016-08-02.
 */
public class ExampleEvents {
    class TestEvent extends Event {
        private int testInt;

        public TestEvent(int testInt) {
            this.testInt = testInt;
        }

        public int getTestInt(){
            return testInt;
        }

    }
    class TestReplyEvent extends Event {
        int val;

        public TestReplyEvent(int val) {
            this.val = val;
        }
        public String toString() {
            return "TestReplyEvent with val: " + val;
        }
    }


    @EventListener
    public TestReplyEvent handle(TestEvent e) {
        return new TestReplyEvent(e.getTestInt());
    }

    @EventListener
    public TestReplyEvent handleAndReply(TestEvent e) {
        return new TestReplyEvent(e.getTestInt());
    }

    @EventListener
    public TestReplyEvent handleNoArg(TestEvent e) {
        return new TestReplyEvent(e.getTestInt());
    }
}
