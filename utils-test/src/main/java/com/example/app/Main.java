package com.example.app;

import edu.szyrek.utils.annotation.App;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.events.Event;
import edu.szyrek.utils.events.EventBus;
import edu.szyrek.utils.events.EventBusShutDownEvent;
import edu.szyrek.utils.events.EventListener;
import edu.szyrek.utils.meta.ConfigurableClassLoader;
import lombok.extern.slf4j.Slf4j;

import java.util.Observer;

/**
 * Created by bebebaba on 2016-07-31.
 */
@Slf4j
@App(name = "MainApp", mainApp = true, classLoader = ConfigurableClassLoader.class)
public class Main implements EventListener<EventBusShutDownEvent> {
    @Configurable(key="exampleJSON")
    private static ExampleJSONType myValue;

    @Override
    public void handle(EventBusShutDownEvent event) {}

    static class ExampleEventListener implements EventListener<ExampleEvent> {
        @Override
        public void handle(ExampleEvent event) {
            assert(myValue.toString().equals("Person: Szymon Zyrek"));
            System.out.println("Hello from utils-test");
        }
    }

    public static void main(String args[]) {
        Main main = new Main();
        EventBus.MAIN.registerListener(main);
        EventBus.MAIN.registerListener(new ExampleEventListener());
        EventBus.MAIN.fireEvent(new ExampleEvent());
    }

}
