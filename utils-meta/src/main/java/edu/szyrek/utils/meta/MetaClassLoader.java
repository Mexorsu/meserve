package edu.szyrek.utils.meta;

import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bebebaba on 2016-08-01.
 */
@Slf4j
public abstract class MetaClassLoader extends URLClassLoader {
    private Map<String, Class<?>> classes = new HashMap<>(); //used to cache already defined classes
    private boolean initialized = false;
    protected ClassLoader parent;

    public MetaClassLoader(ClassLoader parent) {
        this(((URLClassLoader)parent).getURLs(), null);
        this.parent = parent;
    }

    public MetaClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);

    }

    protected abstract Class<?> reallyLoad(String className) throws ClassNotFoundException;
    protected abstract void initialize();

    private boolean canLoadFromCache(String className) {
        return classes.containsKey(className);
    }

    private Class<?> loadFromCache(String className) {
        log.trace("Fetching Class '{}' from cache", className);
        return (Class<?>) classes.get(className);
    }

    private void cacheClass(Class<?> clazz) {
        classes.put(clazz.getCanonicalName(), clazz);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        initializeIfNeeded();

        if (canLoadFromCache(name)) {
            return loadFromCache(name);
        } else {
            Class<?> result = reallyLoad(name);
            cacheClass(result);
            return result;
        }
    }

    private void initializeIfNeeded() {
        if (!initialized) {
            initialize();
            this.initialized = true;
        }
    }

}
