package edu.szyrek.utils.meta;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Observable;

import static javafx.scene.input.KeyCode.T;

/**
 * Created by bebebaba on 2016-08-01.
 */
@Slf4j
public class MetaConfiguration {
    public final static String META_CONF_FILE_PROPERTY = "meta.conf";
    public final static String META_CONF_FILE_NAME = "meta.conf";
    protected static JSONParser jsonParser = new JSONParser();
    protected JSONObject metaConfig;
    private File metaConfigFile;

    protected MetaConfiguration() {
        this.metaConfig = new JSONObject();
    }

    public String toJSONString() {
        return this.metaConfig.toJSONString();
    }

    public int size() {
        return metaConfig.size();
    }

    public static MetaConfiguration fromStrean(InputStream stream) {
        MetaConfiguration result = new MetaConfiguration();
        try {
            result.readMetaConfig(stream);
        } catch (IOException | ParseException e) {
            log.error("Failed to load metaconfig from stream {}", stream, e);
        }
        return result;
    }

    public static MetaConfiguration fromFile(File file) {
        MetaConfiguration result = new MetaConfiguration();
        result.metaConfigFile = file;
        if (!result.metaConfigFile.exists()) {
            log.warn("No metaconfiguration file found at {}, creating", result.metaConfigFile.getAbsolutePath());
            tryCreateMetaConfigFile(result.metaConfigFile);
            result.metaConfig = new JSONObject();
        } else {
            try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(result.metaConfigFile)))) {
                result = MetaConfiguration.fromStrean(in);
            } catch (IOException e) {
                log.error("Failed to read metaconfiguration at {}", result.metaConfigFile.getAbsolutePath(), e);
                result.metaConfig = new JSONObject();
            }
        }
        return result;
    }

    public boolean containsKey(String key) {
        return metaConfig.containsKey(key);
    }

    public <T> T get(String key){
        return (T) metaConfig.get(key);
    }

    public <T> void put(String key, T val){
        metaConfig.put(key, val);
    }

    public boolean write() {
        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(metaConfigFile)))) {
            writeMetaConfig(out);
            return true;
        } catch (ParseException  | IOException e) {
            log.error("Failed to write meta.conf to {}",metaConfigFile.getAbsolutePath(), e);
            return false;
        }
    }

    public MetaConfiguration copy() {
        MetaConfiguration result = new MetaConfiguration();
        for (Object key: this.metaConfig.keySet()) {
            result.put((String)key, this.metaConfig.get(key));
        }
        return result;
    }

    public MetaConfiguration merge(MetaConfiguration other) {
        MetaConfiguration result = this.copy();
        for (Object key: other.metaConfig.keySet()) {
            result.put((String) key, other.metaConfig.get(key));
        }
        return result;
    }


    protected static JSONObject loadMetaConfiguration() throws IOException, ParseException {
        DataInputStream stream;
        String metaConfOverride = System.getProperty(META_CONF_FILE_PROPERTY);
        if (metaConfOverride != null) {
            stream = new DataInputStream(new BufferedInputStream(new FileInputStream(new File(metaConfOverride))));
        } else {
            stream = new DataInputStream(new BufferedInputStream(ClassLoader.getSystemResourceAsStream(META_CONF_FILE_NAME)));
        }
        StringBuffer buffer = new StringBuffer();
        while(stream.available()>0){
            buffer.append(stream.readLine());
        }
        return (JSONObject) new JSONParser().parse(buffer.toString());
    }


    private static void tryCreateMetaConfigFile(File metaConfigFile) {
        try {
            metaConfigFile.createNewFile();
        } catch (IOException e) {
            log.error("Failed to create metaConfig file at {}", metaConfigFile.getAbsolutePath(), e);
        }
    }

    private void readMetaConfig(InputStream stream) throws ParseException, IOException {
        DataInputStream in = new DataInputStream(stream);
        StringBuffer sb = new StringBuffer();

        while (in.available() > 0) {
            sb.append(in.readLine());
        }

        // all these lines just because JSONParser is not thread safe :(
        String jsonString = sb.toString();
        synchronized (jsonParser) {
            metaConfig = (JSONObject) jsonParser.parse(jsonString);
        }
    }

    private void writeMetaConfig(DataOutputStream out) throws ParseException, IOException {
        String jsonString = metaConfig.toJSONString();
        out.write(jsonString.getBytes(Charset.defaultCharset()));
    }

}
