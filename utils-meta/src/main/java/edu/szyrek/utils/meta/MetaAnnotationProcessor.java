package edu.szyrek.utils.meta;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.io.*;
import java.lang.annotation.Annotation;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Set;

/**
 * Created by bebebaba on 2016-08-01.
 */
@Slf4j
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public abstract class MetaAnnotationProcessor extends AbstractProcessor {
    private static String metaConfFilePath;

    protected static MetaConfiguration metaConfig;

    static {
        try {
            metaConfFilePath = MetaAnnotationProcessor.class.getResource("/").toURI().getPath() + File.separator + MetaConfiguration.META_CONF_FILE_NAME;
            metaConfig = MetaConfiguration.fromFile(new File(metaConfFilePath));
        } catch (URISyntaxException e) {
            log.error("Failed to load metaconfig at {} ", metaConfFilePath);
        }
    }


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return tryProcess(annotations, roundEnv) && metaConfig.write();

    }

    public abstract boolean tryProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv);

    private TypeElement asTypeElement(TypeMirror typeMirror) {
        Types TypeUtils = this.processingEnv.getTypeUtils();
        return (TypeElement)TypeUtils.asElement(typeMirror);
    }

    protected String getFullName(TypeMirror mirror) throws NoSuchFieldException, IllegalAccessException {
        TypeElement el = asTypeElement(mirror);
        String fullName;
        el.getClass().getDeclaredField("fullname").setAccessible(true);
        fullName = el.getClass().getDeclaredField("fullname").get(el).toString();
        el.getClass().getDeclaredField("fullname").setAccessible(false);
        return fullName;
    }

    protected String getFullName(Element el) throws NoSuchFieldException, IllegalAccessException {
        String fullName;
        el.getClass().getDeclaredField("fullname").setAccessible(true);
        fullName = el.getClass().getDeclaredField("fullname").get(el).toString();
        el.getClass().getDeclaredField("fullname").setAccessible(false);
        return fullName;
    }


}
