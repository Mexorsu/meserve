package edu.szyrek.utils.random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by bebebaba on 2016-08-13.
 */
class TestFloatValue {
    private Float value;
    private Float lowerBound;
    private Float upperBound;

    public TestFloatValue() {
        lowerBound = Random.get(Float.class);
        upperBound = Random.get(Float.class);
        if (upperBound<lowerBound) {
            Float temp = lowerBound;
            temp = lowerBound;
            lowerBound = upperBound;
            upperBound = temp;
        }
        value = new RandomFloat(lowerBound, upperBound).get();
    }

    public boolean isInBounds() {
        return value > lowerBound && value < upperBound;
    }
    public Float getValue() {
        return value;
    }
}
public class RandomFloatUT {
    private static final int TEST_ARR_SIZE = 100;
    public static final double VARIANCE_TRESHOLD = 1.E70;
    public static final double RANDOMNESS_TRESHOLD = 0.95;

    private static final TestFloatValue[] testArr = new TestFloatValue[TEST_ARR_SIZE];


    @BeforeClass
    public static void setupTestArr() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            testArr[i] = new TestFloatValue();
        }
    }

    @Test
    public void testVarianve() {
        double variance = AnalysisUtil.getVariance(Arrays.stream(testArr).mapToDouble(testElem->testElem.getValue()).toArray());
        Assert.assertTrue("Too small variance: "+variance, variance > VARIANCE_TRESHOLD);
    }

    @Test
    public void testRandomness() {
        Float[] values = new Float[testArr.length];
        for (int i = 0; i < testArr.length; i++) {
            values[i] = testArr[i].getValue();
        }
        double randomness = AnalysisUtil.getRandomness(values);
        Assert.assertTrue("Too small randomness: "+randomness, randomness > RANDOMNESS_TRESHOLD);
    }

    @Test
    public void testUpperLowerBound() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Assert.assertTrue(testArr[i].isInBounds());
        }
    }

}