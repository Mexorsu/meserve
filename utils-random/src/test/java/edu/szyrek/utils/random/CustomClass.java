package edu.szyrek.utils.random;

import java.util.Objects;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class CustomClass implements Randomizable<CustomClass> {
    String stringVal;
    Integer intVal;

    public class CustomBounds implements Bounds {
        public StringBounds stringBound;
        public IntegerBounds intBound;
        public CustomBounds(StringBounds stringBound, IntegerBounds integerBound) {
            this.stringBound = stringBound;
            this.intBound = integerBound;
        }

        @Override
        public void checkBounds() throws InvalidBoundException {

        }
    }

    public CustomClass() {}

    @Override
    public void randomize(Bounds bounds) throws InvalidBoundException {
        if (bounds != Bounds.NO_BOUNDS) {
            CustomBounds customBounds = (CustomBounds) bounds;
            this.stringVal = Random.get(String.class, customBounds.stringBound);
            this.intVal = Random.get(Integer.class, customBounds.intBound);
        } else {
            this.stringVal = Random.get(String.class);
            this.intVal = Random.get(Integer.class);
        }
    }

    @Override
    public CustomClass get() {
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj==null||!CustomClass.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        CustomClass other = (CustomClass)obj;
        return Objects.equals(this.stringVal, other.stringVal) && Objects.equals(this.intVal, other.intVal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intVal, stringVal);
    }
    @Override
    public String toString() {
        return "CustomClass: "+stringVal+", "+intVal;
    }

}