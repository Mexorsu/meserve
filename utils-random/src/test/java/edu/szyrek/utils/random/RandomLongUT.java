package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by bebebaba on 2016-08-13.
 */
class TestLongValue {
    private long value;
    private Long lowerBound;
    private Long upperBound;

    public TestLongValue() {
        lowerBound = Random.get(Long.class);
        upperBound = Random.get(Long.class);
        if (upperBound<lowerBound) {
            Long temp = lowerBound;
            temp = lowerBound;
            lowerBound = upperBound;
            upperBound = temp;
        }
        value = new RandomLong(lowerBound, upperBound).get();
    }

    public boolean isInBounds() {
        return value > lowerBound && value < upperBound;
    }
    public Long getValue() {
        return value;
    }
}
public class RandomLongUT {
    private static final int TEST_ARR_SIZE = 100;
    public static final double VARIANCE_TRESHOLD = 1.E35;
    public static final double RANDOMNESS_TRESHOLD = 0.95;

    private static final TestLongValue[] testArr = new TestLongValue[TEST_ARR_SIZE];


    @BeforeClass
    public static void setupTestArr() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            testArr[i] = new TestLongValue();
        }
    }

    @Test
    public void testVariance() {
        double variance = AnalysisUtil.getVariance(Arrays.stream(testArr).mapToLong(testElem->testElem.getValue()).toArray());
        Assert.assertTrue("Too small randomness: "+variance, variance > VARIANCE_TRESHOLD);
    }


    @Test
    public void testRandomness() {
        Long[] values = new Long[testArr.length];
        for (int i = 0; i < testArr.length; i++) {
            values[i] = testArr[i].getValue();
        }
        double randomness = AnalysisUtil.getRandomness(values);
        Assert.assertTrue("Too small randomness: "+randomness, randomness > RANDOMNESS_TRESHOLD);
    }

    @Test
    public void testUpperLowerBound() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Assert.assertTrue(testArr[i].isInBounds());
        }
    }

}
