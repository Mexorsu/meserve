package edu.szyrek.utils.random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class BoundsUT {

    private static final int TEST_ARR_SIZE = 100000;
    private static final Integer[] ZERO_TO_TEN = new Integer[TEST_ARR_SIZE];
    private static final Integer[] NEGATIVE_TEN_TO_ZERO = new Integer[TEST_ARR_SIZE];
    private static final Integer[] NEGATIVE_TEN_TO_TEN = new Integer[TEST_ARR_SIZE];
    private static final Integer[] NEGATIVE_HUNDRED_TO_NEGATIVE_TEN = new Integer[TEST_ARR_SIZE];
    private static final Integer[] TEN_TO_HUNDRED = new Integer[TEST_ARR_SIZE];
    private static final Integer[] UP_TO_TEN = new Integer[TEST_ARR_SIZE];
    private static final Integer[] FROM_TEN_UP = new Integer[TEST_ARR_SIZE];
    private static final Integer[] FROM_NEGATIVE_TEN_TO_HUNDRED = new Integer[TEST_ARR_SIZE];

    @Rule
    public ExpectedException expectation = ExpectedException.none();

    @BeforeClass
    public static void setupArrays() throws InvalidBoundException {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            ZERO_TO_TEN[i] = Random.get(Integer.class, IntegerBounds.fromZeroTo(10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            NEGATIVE_TEN_TO_ZERO[i] = Random.get(Integer.class, IntegerBounds.fromZeroTo(-10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            NEGATIVE_TEN_TO_TEN[i] = Random.get(Integer.class, IntegerBounds.range(-10,10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            NEGATIVE_HUNDRED_TO_NEGATIVE_TEN[i] = Random.get(Integer.class, IntegerBounds.range(-100,-10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            TEN_TO_HUNDRED[i] = Random.get(Integer.class, IntegerBounds.range(10, 100));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            UP_TO_TEN[i] = Random.get(Integer.class, IntegerBounds.boundUpTo(10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            FROM_TEN_UP[i] = Random.get(Integer.class, IntegerBounds.boundFrom(10));
        }
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            FROM_NEGATIVE_TEN_TO_HUNDRED[i] = Random.get(Integer.class, IntegerBounds.boundFrom(-10).upTo(100));
        }
    }

    @Test
    public void testComplexOKString() throws InvalidBoundException {
        HashSet<String> charsets = new HashSet<>();
        charsets.add(RandomString.ALPHANUMERIC);
        charsets.add(RandomString.LETTERS);
        charsets.add(RandomString.NUMBERS);
        charsets.add(RandomString.ALPHAWHITE);
        charsets.add(RandomString.SYMBOLS);
        charsets.add(RandomString.TYPABLE);
        charsets.add(RandomString.LOWERCASE);
        charsets.add(RandomString.UPPERCASE);

        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            String charset = Random.fromSet(charsets);
            Integer length = Random.get(Integer.class, IntegerBounds.range(10,20));
            String val = Random.get(String.class, StringBounds.boundCharset(charset).length(length));
            Assert.assertTrue(val.length()==length);

            for (char character: val.toCharArray()) {
                Assert.assertTrue(charset.contains(Character.valueOf(character).toString()));
            }
        }
    }

    @Test
    public void testComplexOKInteger() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Integer val = FROM_NEGATIVE_TEN_TO_HUNDRED[i];
            Assert.assertTrue(val>=-10&&val<=100);
        }
    }

    @Test
    public void testComplexFail() throws InvalidBoundException {
        expectation.expect(InvalidBoundException.class);
        Random.get(Integer.class, IntegerBounds.boundFrom(-10).upTo(-100));
    }

    @Test
    public void testFringeValues() {
        boolean tenFound = false;
        boolean zeroFound = false;
        boolean negativeHundredFound = false;
        boolean negativeTenFound = false;
        boolean HundredFound = false;

        for (Integer val: ZERO_TO_TEN) {
            if (val==10) {
                tenFound = true;
                if (zeroFound) break;
            } else if (val == 0) {
                zeroFound = true;
                if (tenFound) break;
            }
        }
        Assert.assertTrue(tenFound);
        Assert.assertTrue(zeroFound);
        zeroFound = false;
        tenFound = false;
        for (Integer val: NEGATIVE_TEN_TO_ZERO) {
            if (val==-10) {
                negativeTenFound = true;
                if (zeroFound) break;
            } else if (val == 0) {
                zeroFound = true;
                if (negativeTenFound) break;
            }
        }
        Assert.assertTrue(negativeTenFound);
        Assert.assertTrue(zeroFound);
        negativeTenFound = false;
        zeroFound = false;
        for (Integer val: NEGATIVE_TEN_TO_TEN) {
            if (val==-10) {
                negativeTenFound = true;
                if (tenFound) break;
            } else if (val == 10) {
                tenFound = true;
                if (negativeTenFound) break;
            }
        }
        Assert.assertTrue(negativeTenFound);
        Assert.assertTrue(tenFound);
        negativeTenFound = false;
        tenFound = false;
    }

    @Test
    public void testFromToZero() {
        for (Integer val: ZERO_TO_TEN) {
            Assert.assertTrue(val >= 0 && val <= 10);
        }
        for (Integer val: NEGATIVE_TEN_TO_ZERO) {
            Assert.assertTrue(val <= 0 && val >= -10);
        }
    }

    @Test
    public void testRange() {
        for (Integer val: NEGATIVE_TEN_TO_TEN) {
            Assert.assertTrue(val >= -10 && val <= 10);
        }
        for (Integer val: NEGATIVE_HUNDRED_TO_NEGATIVE_TEN) {
            Assert.assertTrue(val >= -100 && val <= -10);
        }
        for (Integer val: TEN_TO_HUNDRED) {
            Assert.assertTrue(val >= 10 && val <= 100);
        }
    }
    @Test
    public void testUpTo() {
        for (Integer val: UP_TO_TEN) {
            Assert.assertTrue(val < 10);
        }
    }
    @Test
    public void testFromUp() {
        for (Integer val: FROM_TEN_UP) {
            Assert.assertTrue(val > 10);
        }
    }
}
