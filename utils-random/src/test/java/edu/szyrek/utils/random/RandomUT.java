package edu.szyrek.utils.random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.omg.CORBA.DoubleHolder;
import org.omg.CORBA.OBJ_ADAPTER;

import java.util.HashSet;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomUT {
    private static final int TEST_ARR_SIZE = 100;
    private static final HashSet<Object> testSet = new HashSet<>();

    static class TestGenerator implements RandomGenerator<Object> {
        @Override
        public Object generateNext() {
            return Random.fromSet(testSet);
        }
    }
    private final static TestGenerator generator = new TestGenerator();

    @BeforeClass
    public static void setupTestSet() {
        for (int i = 0; i <TEST_ARR_SIZE; i++) {
            testSet.add(Random.get(Integer.class));
        }
        for (int i = 0; i <TEST_ARR_SIZE; i++) {
            testSet.add(Random.get(Double.class));
        }
        for (int i = 0; i <TEST_ARR_SIZE; i++) {
            testSet.add(Random.get(String.class));
        }
    }

    @Test
    public void testGetFromSet() {
        Object[] fromSet = new Object[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++) {
            Object nextValue = Random.fromSet(testSet);
            Assert.assertTrue(testSet.contains(nextValue));
            fromSet[i] = nextValue;
        }
        double randomness = AnalysisUtil.getRandomness(fromSet);
        Assert.assertTrue(randomness>fromSet.length/testSet.size());
    }

    @Test
    public void testGetFromGenerator() {
        Object[] fromGenerator = new Object[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++) {
            Object nextValue = Random.fromGenerator(generator);
            Assert.assertTrue(testSet.contains(nextValue));
            fromGenerator[i] = nextValue;
        }
        double randomness = AnalysisUtil.getRandomness(fromGenerator);
        Assert.assertTrue(randomness>0.50);
    }



    /*
     * !!Possible false positive!!
     * This test assumes a specific treshold value for randomeness which can vary between machines maybe?
     *
     */
    @Test
    public void testIntegerRandom() {
        int[] testArr = new int[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++){
            testArr[i] = Random.get(Integer.class);
        }
        double variance = AnalysisUtil.getVariance(testArr);
        Assert.assertTrue("Too small randomness: "+ variance, variance > RandomIntegerUT.VARIANCE_TRESHOLD);
    }
    /*
     * !!Possible false positive!!
     * This test assumes a specific treshold value for randomeness which can vary between machines maybe?
     *
     */
    @Test
    public void testShortRandom() {
        short[] testArr = new short[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++){
            testArr[i] = Random.get(Short.class);
        }
        double variance = AnalysisUtil.getVariance(testArr);
        Assert.assertTrue("Too small randomness: "+ variance, variance > RandomShortUT.VARIANCE_TRESHOLD);
    }

    /*
     * !!Possible false positive!!
     * This test assumes a specific treshold value for randomeness which can vary between machines maybe?
     *
     */
    @Test
    public void testLongRandom() {
        long[] testArr = new long[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++){
            testArr[i] = Random.get(Long.class);
        }
        double variance = AnalysisUtil.getVariance(testArr);
        Assert.assertTrue("Too small randomness: "+ variance, variance > RandomLongUT.VARIANCE_TRESHOLD);
    }

    /*
     * !!Possible false positive!!
     * This test assumes a specific treshold value for randomeness which can vary between machines maybe?
     *
     */
    @Test
    public void testDoubleRandom() {
        double[] testArr = new double[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++){
            testArr[i] = Random.get(Double.class);
        }
        double variance = AnalysisUtil.getVariance(testArr);
        Assert.assertTrue("Too small randomness: "+ variance, variance > RandomFloatUT.VARIANCE_TRESHOLD);
    }

    /*
     * !!Possible false positive!!
     * This test assumes a specific treshold value for randomeness which can vary between machines maybe?
     *
     */
    @Test
    public void testFloatRandom() {
        float[] testArr = new float[TEST_ARR_SIZE];
        for (int i = 0; i <TEST_ARR_SIZE; i++){
            testArr[i] = Random.get(Float.class);
        }
        double variance = AnalysisUtil.getVariance(testArr);
        Assert.assertTrue("Too small randomness: "+ variance, variance > RandomFloatUT.VARIANCE_TRESHOLD);
    }

    /*
     * !!Possible false positive!!
     * just checks if two following random strings are equal, given default length of 10 signs
     * and default charset which holds all typable characters it's extremely unlikely that
     * this would randomly fail but honestly it theoretically can...
     */
    @Test
    public void testStringRandom() {
        String random1 = Random.get(String.class);
        String random2 = Random.get(String.class);
        Assert.assertNotEquals("Weak randomization, two following random strings were identical: "+random1, random1, random2);
    }

    /*
     * !!Possible false positive!!
     * just checks if two following random objects are equal, given that CustomClass equality
     * is determined by a random string + random int it's extremely unlikely that
     * this would randomly fail but honestly it theoretically can...
     */
    @Test
    public void testCustomRandom() {
        CustomClass random1 = Random.get(CustomClass.class);
        CustomClass random2 = Random.get(CustomClass.class);
        Assert.assertNotEquals("Weak randomization, two following random objects were identical: "+random1, random1, random2);
    }
}
