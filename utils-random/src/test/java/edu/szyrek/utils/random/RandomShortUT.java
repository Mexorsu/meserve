package edu.szyrek.utils.random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by bebebaba on 2016-08-13.
 */
class TestShortValue {
    private short value;
    private short lowerBound;
    private short upperBound;

    public TestShortValue() {
        lowerBound = Random.get(Short.class);
        upperBound = Random.get(Short.class);
        if (upperBound<lowerBound) {
            short temp = lowerBound;
            temp = lowerBound;
            lowerBound = upperBound;
            upperBound = temp;
        }
        value = new RandomShort(lowerBound, upperBound).get();
    }

    public boolean isInBounds() {
        return value > lowerBound && value < upperBound;
    }
    public Short getValue() {
        return value;
    }
}
public class RandomShortUT {
    private static final int TEST_ARR_SIZE = 100;
    public static final double VARIANCE_TRESHOLD = 1.E8;
    public static final double RANDOMNESS_TRESHOLD = 0.95;

    private static final TestShortValue[] testArr = new TestShortValue[TEST_ARR_SIZE];


    @BeforeClass
    public static void setupTestArr() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            testArr[i] = new TestShortValue();
        }
    }

    @Test
    public void testVariance() {
        double variance = AnalysisUtil.getVariance(Arrays.stream(testArr).mapToInt(testElem->testElem.getValue()).toArray());
        Assert.assertTrue("Too small randomness: "+variance, variance > VARIANCE_TRESHOLD);
    }

    @Test
    public void testRandomness() {
        Short[] values = new Short[testArr.length];
        for (int i = 0; i < testArr.length; i++) {
            values[i] = testArr[i].getValue();
        }
        double randomness = AnalysisUtil.getRandomness(values);
        Assert.assertTrue("Too small randomness: "+randomness, randomness > RANDOMNESS_TRESHOLD);
    }

    @Test
    public void testUpperLowerBound() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Assert.assertTrue(testArr[i].isInBounds());
        }
    }

}
