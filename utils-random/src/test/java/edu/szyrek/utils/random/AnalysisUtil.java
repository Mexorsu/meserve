package edu.szyrek.utils.random;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class AnalysisUtil {

    private static double getMean(double[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum/arr.length;
    }

    private static double getMean(float[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum/arr.length;
    }

    private static double getMean(short[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum/arr.length;
    }

    private static double getMean(int[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum/arr.length;
    }

    private static double getMean(long[] arr) {
        double sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum/arr.length;
    }

    public static double getVariance(short[] arr) {
        double mean = getMean(arr);
        double size = (double) arr.length;
        double sum = 0;

        for (int i = 0; i < arr.length; i++){
            sum += ((arr[i]-mean)*(arr[i]-mean));
        }
        return (1/(size-1))*sum;
    }

    public static double getVariance(int[] arr) {
        double mean = getMean(arr);
        double size = (double) arr.length;
        double sum = 0;

        for (int i = 0; i < arr.length; i++){
            sum += ((arr[i]-mean)*(arr[i]-mean));
        }
        return (1/(size-1))*sum;
    }

    public static double getVariance(long[] arr) {
        double mean = getMean(arr);
        double size = (double) arr.length;
        double sum = 0;

        for (int i = 0; i < arr.length; i++){
            sum += ((arr[i]-mean)*(arr[i]-mean));
        }
        return (1/(size-1))*sum;
    }
    public static double getVariance(float[] arr) {
        double mean = getMean(arr);
        double size = (double) arr.length;
        double sum = 0;

        for (int i = 0; i < arr.length; i++){
            sum += ((arr[i]-mean)*(arr[i]-mean));
        }
        return (1/(size-1))*sum;
    }
    public static double getVariance(double[] arr) {
        double mean = getMean(arr);
        double size = (double) arr.length;
        double sum = 0;

        for (int i = 0; i < arr.length; i++){
            sum += ((arr[i]-mean)*(arr[i]-mean));
        }
        return (1/(size-1))*sum;
    }

    public static <T> double getRandomness(T[] arr){
        HashMap<T, Integer> distribution = new HashMap<>();
        for (T el: arr){
            distribution.put(el, 0);
        }
        for (T el: arr){
            distribution.put(el, distribution.get(el)+1);
        }
        int[] distArr = new int[distribution.size()];
        int i = 0;
        for (T key: distribution.keySet()) {
            distArr[i++] = distribution.get(key);
        }
        return 1.0 - getVariance(distArr);
    }

}
