package edu.szyrek.utils.random;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by bebebaba on 2016-08-13.
 */

class TestValue {
    private int value;
    private int lowerBound;
    private int upperBound;

    public TestValue() {
        lowerBound = Random.get(Short.class);
        upperBound = Random.get(Integer.class);
        if (upperBound<lowerBound) {
            int temp = lowerBound;
            temp = lowerBound;
            lowerBound = upperBound;
            upperBound = temp;
        }
        value = new RandomInteger(lowerBound, upperBound).get();
    }

    public boolean isInBounds() {
        return value > lowerBound && value < upperBound;
    }
    public Integer getValue() {
        return value;
    }
}
public class RandomIntegerUT {
    private static final int TEST_ARR_SIZE = 100;
    public static final double VARIANCE_TRESHOLD = 1.E17;
    public static final double RANDOMNESS_TRESHOLD = 0.95;

    private static final TestValue[] testArr = new TestValue[TEST_ARR_SIZE];


    @BeforeClass
    public static void setupTestArr() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            testArr[i] = new TestValue();
        }
    }

    @Test
    public void testVariance() {
        double variance = AnalysisUtil.getVariance(Arrays.stream(testArr).mapToInt(testElem->testElem.getValue()).toArray());
        Assert.assertTrue("Too small randomness: "+variance, variance > VARIANCE_TRESHOLD);
    }


    @Test
    public void testRandomness() {
        Integer[] values = new Integer[testArr.length];
        for (int i = 0; i < testArr.length; i++) {
            values[i] = testArr[i].getValue();
        }
        double randomness = AnalysisUtil.getRandomness(values);
        Assert.assertTrue("Too small randomness: "+randomness, randomness > RANDOMNESS_TRESHOLD);
    }

    @Test
    public void testUpperLowerBound() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Assert.assertTrue(testArr[i].isInBounds());
        }
    }

}