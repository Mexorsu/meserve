package edu.szyrek.utils.random;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by bebebaba on 2016-08-13.
 */
class TestDoubleValue {
    private Double value;
    private Double lowerBound;
    private Double upperBound;

    public TestDoubleValue() {
        lowerBound = Random.get(Double.class);
        upperBound = Random.get(Double.class);
        if (upperBound<lowerBound) {
            Double temp = lowerBound;
            temp = lowerBound;
            lowerBound = upperBound;
            upperBound = temp;
        }
        value = new RandomDouble(lowerBound, upperBound).get();
    }

    public boolean isInBounds() {
        return value > lowerBound && value < upperBound;
    }
    public Double getValue() {
        return value;
    }
}
public class RandomDoubleUT {
    private static final int TEST_ARR_SIZE = 100;
    public static final double VARIANCE_TRESHOLD = Double.MAX_VALUE; //1.E308w;
    public static final double RANDOMNESS_TRESHOLD = 0.95;

    private static final TestDoubleValue[] testArr = new TestDoubleValue[TEST_ARR_SIZE];


    @BeforeClass
    public static void setupTestArr() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            testArr[i] = new TestDoubleValue();
        }
    }

    @Test
    public void testVarianve() {
        double variance = AnalysisUtil.getVariance(Arrays.stream(testArr).mapToDouble(testElem->testElem.getValue()).toArray());
        Assert.assertTrue("Too small variance: "+variance, variance > VARIANCE_TRESHOLD);
    }

    @Test
    public void testRandomness() {
        Double[] values = new Double[testArr.length];
        for (int i = 0; i < testArr.length; i++) {
            values[i] = testArr[i].getValue();
        }
        double randomness = AnalysisUtil.getRandomness(values);
        Assert.assertTrue("Too small randomness: "+randomness, randomness > RANDOMNESS_TRESHOLD);
    }

    @Test
    public void testUpperLowerBound() {
        for (int i = 0; i < TEST_ARR_SIZE; i++) {
            Assert.assertTrue(testArr[i].isInBounds());
        }
    }

}