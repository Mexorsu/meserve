package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public interface Randomizable<T> {
    public T get();
    public void randomize(Bounds bounds) throws InvalidBoundException;
}
