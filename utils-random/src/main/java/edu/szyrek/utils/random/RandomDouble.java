package edu.szyrek.utils.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomDouble implements Randomizable<Double> {
    private final static java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());

    private Double _value;

    public RandomDouble() {
        randomize(Bounds.NO_BOUNDS);
    }
    public RandomDouble(Bounds bounds) {
        randomize(bounds);
    }

    public RandomDouble(Double lowerBound, Double upperBound) {
        this._value = ThreadLocalRandom.current().nextDouble(lowerBound, upperBound);
    }

    @Override
    public void randomize(Bounds bounds) {
        this._value = ThreadLocalRandom.current().nextDouble(Double.MIN_VALUE, Double.MAX_VALUE);
    }

    @Override
    public Double get() {
        return _value;
    }
}