package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public interface RandomGenerator<T> {
    T generateNext();
}
