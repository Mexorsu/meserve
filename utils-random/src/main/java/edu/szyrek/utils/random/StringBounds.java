package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class StringBounds implements Bounds {
    private String charset;
    private int length;

    public static final StringBounds DEFAULT = new StringBounds(RandomString.TYPABLE);

    public static StringBounds boundCharset(String charset) {
        return new StringBounds(charset);
    }

    public static StringBounds boundLength(int length) {
        return new StringBounds(length);
    }

    public static StringBounds bound(int length, String charset) {
        return new StringBounds(length, charset);
    }

    public static final StringBounds NO_BOUNDS = new StringBounds();

    private StringBounds(){}

    public StringBounds charset(String charset) {
        this.charset = charset;
        return this;
    }

    public StringBounds length(int length) {
        this.length = length;
        return this;
    }

    private StringBounds(int length) {
        this(length, RandomString.TYPABLE);
    }
    private StringBounds(String charset) {
        this(RandomString.DEFAULT_LENGTH, charset);
    }

    private StringBounds(int length, String charset) {
        this.charset = charset;
        this.length = length;
    }

    public String getCharset() {
        return charset;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void checkBounds() throws InvalidBoundException {}
}
