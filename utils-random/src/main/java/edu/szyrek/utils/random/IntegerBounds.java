package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class IntegerBounds implements Bounds<Integer> {
    private int lowerBound;
    private int upperBound;

    public static final IntegerBounds NO_BOUNDS = new IntegerBounds();

    public static IntegerBounds boundUpTo(int highestValue) {
        return new IntegerBounds().from(Integer.MIN_VALUE).upTo(highestValue);
    }

    public IntegerBounds upTo(int highestValue) {
        this.upperBound = highestValue;
        return this;
    }

    public static IntegerBounds boundFrom(int lowestValue) {
        return new IntegerBounds().from(lowestValue).upTo(Integer.MAX_VALUE);
    }

    public IntegerBounds from(int lowestValue) {
        this.lowerBound = lowestValue;
        this.upperBound = Integer.MAX_VALUE;
        return this;
    }

    public static IntegerBounds fromZeroTo(int value) {
        if (value > 0) {
            return new IntegerBounds(0, value);
        } else if (value < 0) {
            return new IntegerBounds(value, 0);
        } else {
            return new IntegerBounds(0,0);
        }
    }

    public static IntegerBounds range(int lower, int upper) {
        return new IntegerBounds(lower, upper);
    }

    private IntegerBounds() {
        lowerBound = Integer.MIN_VALUE;
        upperBound = Integer.MAX_VALUE;

    }

    @Override
    public void checkBounds() throws InvalidBoundException {
        if (!isValid()) {
            throw new InvalidBoundException();
        }
    }

    private boolean isValid() {return lowerBound<upperBound;}


    private IntegerBounds(int lower, int upper) {
        this.lowerBound = lower;
        this.upperBound = upper;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

}
