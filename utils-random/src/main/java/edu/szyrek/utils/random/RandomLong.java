package edu.szyrek.utils.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomLong implements Randomizable<Long> {
    private final static java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());

    private Long _value;

    public RandomLong() {
        randomize(Bounds.NO_BOUNDS);
    }

    public RandomLong(long lowerBound, long upperBound) {
        this._value = nextLong(upperBound-lowerBound)+lowerBound;
    }

    public RandomLong(Bounds bounds) {
        this.randomize(bounds);
    }

    @Override
    public void randomize(Bounds bounds) {
        this._value = RANDOM.nextLong();
    }

    private static long nextLong(long n) {
        long bound = Math.abs(n);
        return ThreadLocalRandom.current().nextLong(bound);
    }

    @Override
    public Long get() {
        return _value;
    }
}