package edu.szyrek.utils.random;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

/**
 * Created by bebebaba on 2016-08-13.
 */

public class Random {
    private static final java.util.Random _random = new java.util.Random(System.currentTimeMillis());

    public static <E> E fromSet(Set<E> values){
        return (E) values.toArray()[new RandomInteger(0, values.size()-1).get()];
    }

    public static <E> E fromGenerator(RandomGenerator<E> generator) {
        return generator.generateNext();
    }

    public static <E> E get(Class<? extends E> type, Bounds bounds) throws InvalidBoundException {
        if (Randomizable.class.isAssignableFrom(type)) {
            try {
                Randomizable randomInstance = (Randomizable) type.getDeclaredConstructor().newInstance();
                randomInstance .randomize(bounds);
                return (E) randomInstance;
            } catch (InvocationTargetException|IllegalAccessException |InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvalidBoundException e) {
                e.printStackTrace();
            }
        } else if (String.class.isAssignableFrom(type)) {
                return (E) new RandomString(bounds).get();
            } else if (Integer.class.isAssignableFrom(type)) {
                return (E) new RandomInteger(bounds).get();
            } else if (Long.class.isAssignableFrom(type)) {
                return (E) new RandomLong().get();
            } else if (Short.class.isAssignableFrom(type)) {
                return (E) new RandomShort().get();
            } else if (Double.class.isAssignableFrom(type)) {
                return (E) new RandomDouble().get();
            } else if (Float.class.isAssignableFrom(type)) {
                return (E) new RandomFloat().get();
            } else if (Boolean.class.isAssignableFrom(type)) {
                return (E) Boolean.valueOf(_random.nextBoolean());
            }
        throw new IllegalArgumentException("Cannot randomize "+type.getCanonicalName());

    }

    public static <E> E get(Class<? extends E> type){
        try {
            return get(type, Bounds.NO_BOUNDS);
        } catch (InvalidBoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
