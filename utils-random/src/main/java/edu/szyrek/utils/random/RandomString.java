package edu.szyrek.utils.random;

import java.util.Random;

/**
 * Created by bebebaba on 2016-08-12.
 */
public class RandomString implements Randomizable<String> {
    public final static String LOWERCASE = "qwertyuioplkjhgfdsazxcvbnm";
    public final static String UPPERCASE = "QWERTYUIOPLKJHGFDSAZXCVBNM";
    public final static String NUMBERS = "0123456789";
    public final static String SYMBOLS = "~`!@#$%^&*()-_=+\\|[{]};:',<.>/?";
    public final static String WHITESPACE = " \n\t";
    public final static String LETTERS = LOWERCASE + UPPERCASE;
    public final static String ALPHANUMERIC = LETTERS + NUMBERS;
    public final static String ALPHAWHITE = ALPHANUMERIC + WHITESPACE;
    public final static String TYPABLE = ALPHANUMERIC + SYMBOLS + WHITESPACE;

    public enum Charset {

        LOWERCASE, UPPERCASE, NUMBERS, SYMBOLS, WHITESPACE, ALPHANUMERIC, ALPHAWHITE, TYPABLE;
        public String getCharset(){
            switch (this) {
                case LOWERCASE:
                    return RandomString.LOWERCASE;
                case UPPERCASE:
                    return RandomString.UPPERCASE;
                case NUMBERS:
                    return RandomString.NUMBERS;
                case SYMBOLS:
                    return RandomString.SYMBOLS;
                case WHITESPACE:
                    return RandomString.WHITESPACE;
                case ALPHANUMERIC:
                    return RandomString.ALPHANUMERIC;
                case ALPHAWHITE:
                    return RandomString.ALPHAWHITE;
                case TYPABLE:
                    return RandomString.TYPABLE;
            }
            return "#";
        }
    }
    private final static Random RANDOM = new Random(System.currentTimeMillis());
    public final static int DEFAULT_LENGTH = 10;

    private String value;

    public RandomString(int length, Charset charset) {
        this.value = getNext(charset.getCharset(), length);
    }

    public RandomString(Charset charset) {
        this.value = getNext(charset.getCharset());
    }

    public RandomString() {
        this.randomize(Bounds.NO_BOUNDS);
    }

    public RandomString(Bounds bounds) {
        this.randomize(bounds);
    }

    @Override
    public void randomize(Bounds bounds) {
        if (bounds != Bounds.NO_BOUNDS) {
            StringBounds stringBounds = (StringBounds) bounds;

            this.value = getNext(stringBounds.getCharset(), stringBounds.getLength());
        } else {
            this.value = getNext();
        }
    }

    @Override
    public String get() {
        return value;
    }

    public String get(Bounds bounds) {
        return value;
    }

    public String toString() {
        return value;
    }

    public static String getNext() {
        return getNext(DEFAULT_LENGTH);
    }

    public static String getNext(String charset) {
        return getNext(charset, DEFAULT_LENGTH);
    }

    private static char getChar(String charset) {
        return charset.charAt(RANDOM.nextInt(charset.length()));
    }

    public static String getNext(int length) {
        return getNext(TYPABLE, length);
    }

    public static String getNext(String charset, int length) {
        StringBuffer buffer = new StringBuffer();
        while(buffer.length()<length){
            buffer.append(getChar(charset));
        }
        return buffer.toString();
    }

    public static String getLetters(int length) {
        return getNext(LETTERS, length);
    }

    public static String getLetters() {
        return getNext(LETTERS, DEFAULT_LENGTH);
    }

    public static String getAphanumeric(int length) {
        return getNext(ALPHANUMERIC, length);
    }

    public static String getAphanumeric() {
        return getNext(ALPHANUMERIC, DEFAULT_LENGTH);
    }

    public static String getText(int length) {
        return getNext(ALPHAWHITE, length);
    }

    public static String getText() {
        return getNext(ALPHAWHITE, DEFAULT_LENGTH);
    }

}
