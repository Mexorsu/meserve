package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomInteger implements Randomizable<Integer> {
    private final static java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());

    private Integer _value;


    public RandomInteger(Bounds bounds) throws InvalidBoundException {
        randomize(bounds);
    }

    public RandomInteger() throws InvalidBoundException {
        randomize(Bounds.NO_BOUNDS);
    }

    public RandomInteger(int lowerBound, int upperBound) {
        int bound = upperBound-lowerBound;
        if (bound<0) {
            bound = Math.abs(bound);
        }
        this._value = RANDOM.nextInt(bound+1)+lowerBound;
    }

    @Override
    public void randomize(Bounds bounds) throws InvalidBoundException {
        if (bounds != Bounds.NO_BOUNDS) {
            IntegerBounds intBounds = (IntegerBounds) bounds;
            intBounds.checkBounds();
            int bound = intBounds.getUpperBound()-intBounds.getLowerBound();
            if (bound<0) {
                bound = Math.abs(bound);
            }
            this._value = RANDOM.nextInt(bound+1)+intBounds.getLowerBound();
        } else {
            this._value = RANDOM.nextInt();
        }
    }

    @Override
    public Integer get() {
        return _value;
    }
}
