package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomShort implements Randomizable<Short> {
    private final static java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());

    private Short _value;

    public RandomShort() {
        randomize(Bounds.NO_BOUNDS);
    }

    public RandomShort(Short lowerBound, Short upperBound) {
        this._value = Integer.valueOf(RANDOM.nextInt((int)upperBound-(int)lowerBound)+(int)lowerBound).shortValue();
    }


    public RandomShort(Bounds bounds) {
        this.randomize(bounds);
    }

    @Override
    public void randomize(Bounds bounds) {
        this._value = Integer.valueOf(RANDOM.nextInt(Short.MAX_VALUE-Short.MIN_VALUE)+Short.MIN_VALUE).shortValue();
    }

    @Override
    public Short get() {
        return _value;
    }
}