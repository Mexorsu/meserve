package edu.szyrek.utils.random;

/**
 * Created by bebebaba on 2016-08-13.
 */
public interface Bounds<T> {
    class NoBounds implements Bounds<Object> {
        @Override
        public void checkBounds() throws InvalidBoundException {}
    }
    public NoBounds NO_BOUNDS = new NoBounds();
    public void checkBounds() throws InvalidBoundException;
}
