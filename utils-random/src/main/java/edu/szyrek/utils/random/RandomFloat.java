package edu.szyrek.utils.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class RandomFloat implements Randomizable<Float> {
    private final static java.util.Random RANDOM = new java.util.Random(System.currentTimeMillis());

    private Float _value;

    public RandomFloat(Bounds bounds) {
        randomize(bounds);
    }
    public RandomFloat() {
        randomize(Bounds.NO_BOUNDS);
    }

    public RandomFloat(Float lowerBound, Float upperBound) {
        this._value = Double.valueOf(ThreadLocalRandom.current().nextDouble(lowerBound, upperBound)).floatValue();
    }

    @Override
    public void randomize(Bounds bounds) {
        this._value = Double.valueOf(ThreadLocalRandom.current().nextDouble(Float.MIN_VALUE, Float.MAX_VALUE)).floatValue();
    }

    @Override
    public Float get() {
        return _value;
    }
}