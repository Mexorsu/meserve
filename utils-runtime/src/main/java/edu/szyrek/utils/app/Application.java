package edu.szyrek.utils.app;

import edu.szyrek.utils.bootstrap.Bootstraper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URLClassLoader;

/**
 * Created by bebebaba on 2016-07-17.
 */

@Slf4j
public class Application {
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    public final static String DEFAULT_CLASS_LOADER_LABEL = "default";
    private final static URLClassLoader systemClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    private final static String DEFAULT_CLASS_LOADER_NAME = ClassLoader.getSystemClassLoader().getClass().getCanonicalName();

    public static Application instance;
    public static AppConfig config = AppConfig.getInstance();

    protected Application(String appName) {
        if (instance!=null) throw new IllegalStateException("Application already created!");
        instance = this;
    }

//    public Application(String appName, ClassLoader cl) {
//        if (instance!=null) throw new IllegalStateException("Application already created!");
//        instance = this;
//        classLoaderName = cl.getClass().getCanonicalName();
//    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        if(args.length>0){
            config.put(AppConfig.MAIN_APP, args[0]);
        }

        String mainClassName = getMainClassName();
        Class<?> mainClass = systemClassLoader.loadClass(mainClassName);

        Method mainMethod = mainClass.getMethod("main", String[].class);
        log.trace("Starting application {}", mainClass.getSimpleName());
        tryRunMain(mainMethod, args);
    }

    private static String getMainClassName() throws ClassNotFoundException {
        return (String) getMainApp().get("class");
    }

    private static JSONObject getMainApp() {
        JSONArray mainClasses = config.get(AppConfig.MAIN_CLASS);
        if (mainClasses.size()==1) {
            return (JSONObject) mainClasses.get(0);
        } else if (config.containsKey(AppConfig.MAIN_APP)){
            for (Object clazz: mainClasses) {
                JSONObject classObject = (JSONObject) clazz;
                if (config.get(AppConfig.MAIN_APP).equals(classObject.get("appName"))){
                    return classObject;
                }
            }
            throw new RuntimeException("No @App registered with name "+config.get(AppConfig.MAIN_APP));
        } else {
            for (Object clazz: mainClasses) {
                JSONObject classObject = (JSONObject) clazz;
                if ((boolean)classObject.get("default")==true){
                    return classObject;
                }
            }
            throw new RuntimeException("Multiple @App's found and no defaultApp set. Please pass target app name argument on cmd line");
        }
    }

    private static String getClassLoaderName() {
        String classLoader = (String) getMainApp().get("classLoader");
        if (classLoader.equals(DEFAULT_CLASS_LOADER_LABEL)) {
            return DEFAULT_CLASS_LOADER_NAME;
        } else {
            return classLoader;
        }
    }

    public static void tryRunMain(Method mainMethod, String[] args) {
        try {
            runMain(mainMethod, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("Failed to run {}.{} with args {}", mainMethod.getDeclaringClass().getCanonicalName(), mainMethod.getName(), args, e);
        } catch (URISyntaxException|IOException e) {
            log.error("Failed to bootstrap {}", mainMethod.getDeclaringClass().getCanonicalName(), e);
        }
    }

    public static void runMain(Method mainMethod, String[] args) throws InvocationTargetException, IllegalAccessException, IOException, URISyntaxException {
        String classLoaderName = getClassLoaderName();
        if (ClassLoader.getSystemClassLoader().getClass().getCanonicalName()
                .equals(classLoaderName)) {
            mainMethod.invoke(null, new Object[]{args});
        } else {
            log.trace("Bootstrapping application {}",mainMethod.getDeclaringClass().getCanonicalName());
            Bootstraper.bootstrap(args, classLoaderName);
        }
    }
}
