package edu.szyrek.utils.app;

import edu.szyrek.utils.meta.MetaConfiguration;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by bebebaba on 2016-07-27.
 */
public class AppFilesLocator {

    public static String getJarPath() throws URISyntaxException, IOException {
        String japPath = ClassLoader.getSystemResource(MetaConfiguration.META_CONF_FILE_NAME).toURI().toString();
        String prefix = "file:/";
        if (japPath.contains(prefix)){
            japPath = japPath.substring(japPath.indexOf(prefix)+prefix.length(), japPath.length());
        }

        if (japPath.contains("!")) {
            japPath = japPath.substring(0, japPath.lastIndexOf("!"));
        } else {
            japPath = japPath.substring(0, japPath.length()-MetaConfiguration.META_CONF_FILE_NAME.length());
        }
        return japPath;
    }
}
