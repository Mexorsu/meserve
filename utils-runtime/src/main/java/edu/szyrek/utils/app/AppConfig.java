package edu.szyrek.utils.app;

import edu.szyrek.utils.meta.MetaConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;

import java.io.IOException;

@Slf4j
public class AppConfig extends MetaConfiguration {
    public static final String MAIN_APP = "MAIN_APP";
    public static final String MAIN_CLASS = "mainClass";
    public static final String CLASS_LOADER = "classLoader";

    protected AppConfig() {
        super();
    }

    public static AppConfig getInstance() {
        AppConfig result = new AppConfig();
        try {
            result.metaConfig = loadMetaConfiguration();
        } catch (ParseException | IOException e) {
            log.error("Failed to load default meta.conf from jar file", e);
        }
        return result;
    }
}
