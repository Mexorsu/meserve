package edu.szyrek.utils.runtime;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by bebebaba on 2016-07-17.
 */
public class ComponentClassLoader extends URLClassLoader {
    private Hashtable classes = new Hashtable();
    private Map<AtomicID, Class<? extends ClassLoader>> classLoaders;
    private ClassLoader parent;


    public ComponentClassLoader(ClassLoader parent) {
        this(((URLClassLoader)parent).getURLs(), null);
        this.parent = parent;
    }

    public ComponentClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

}
