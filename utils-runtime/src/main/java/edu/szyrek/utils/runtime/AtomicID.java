package edu.szyrek.utils.runtime;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by bebebaba on 2016-07-17.
 */
@Slf4j
public class AtomicID {
    private static final AtomicLong _lastAssigned = new AtomicLong(0);
    private final long _ID = _lastAssigned.getAndIncrement();

    private AtomicID() {}

    public static AtomicID getNext() {
        AtomicID res = new AtomicID();
        log.trace("AtomicID[{}]", res._ID);
        return res;
    }

    public long raw() {
        return _ID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj==null || !this.getClass().isAssignableFrom(obj.getClass())) return false;
        AtomicID other = (AtomicID) obj;
        return this._ID == other._ID;
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
