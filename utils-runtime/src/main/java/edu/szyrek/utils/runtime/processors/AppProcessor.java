package edu.szyrek.utils.runtime.processors;

import edu.szyrek.utils.annotation.App;
import edu.szyrek.utils.app.AppConfig;
import edu.szyrek.utils.app.Application;
import edu.szyrek.utils.meta.MetaAnnotationProcessor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import java.util.Objects;
import java.util.Set;

/**
 * Created by bebebaba on 2016-07-27.
 */
@Slf4j
@SupportedAnnotationTypes({"edu.szyrek.utils.annotation.App"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class AppProcessor extends MetaAnnotationProcessor {
    private static JSONArray mainClasses = new JSONArray();

    static {
        mainClasses = metaConfig.containsKey(AppConfig.MAIN_CLASS) ? (JSONArray) metaConfig.get(AppConfig.MAIN_CLASS) : mainClasses;
    }

    @Override
    public boolean tryProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        for (TypeElement annotation : annotations) {
            for (Element el : roundEnv.getElementsAnnotatedWith(annotation)) {
                if (App.class.getSimpleName().equals(annotation.getSimpleName().toString())) {

                    App appAnnotation = el.getAnnotation(App.class);

                    String classLoaderName = Application.DEFAULT_CLASS_LOADER_LABEL;
                    try {
                        appAnnotation.classLoader();
                    } catch (MirroredTypeException e) {
                        String vale = e.getTypeMirror().toString();
                        if (!vale.equals(DefaultClassLoader.class.getCanonicalName())){
                            classLoaderName = e.getTypeMirror().toString();
                        }
                    }
                    addApp(appAnnotation.name(), el, appAnnotation.mainApp(), classLoaderName);
                }
            }
        }

        metaConfig.put(AppConfig.MAIN_CLASS, mainClasses);
        return true;
    }

    private static void addApp(String name, Element classElement, boolean isMainApp, String classLoaderName) {
        String fullName = null;
        try {
            classElement.getClass().getDeclaredField("fullname").setAccessible(true);
            fullName = classElement.getClass().getDeclaredField("fullname").get(classElement).toString();
            classElement.getClass().getDeclaredField("fullname").setAccessible(false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        JSONObject mainClassObj = new JSONObject();
        mainClassObj.put("appName", name);
        mainClassObj.put("class", fullName == null ? classElement.getSimpleName().toString() : fullName);
        mainClassObj.put("default", isMainApp);
        mainClassObj.put(AppConfig.CLASS_LOADER, classLoaderName );
        mainClasses.add(mainClassObj);
    }

}
