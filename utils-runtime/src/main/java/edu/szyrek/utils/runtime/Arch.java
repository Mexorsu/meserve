package edu.szyrek.utils.runtime;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by szyzyrek on 6/12/16.
 */


@Slf4j
public class Arch {

    enum OperationSystem {
        WINDOWS, LINUX, OSX, UNDEFINED
    }

    enum Architecture {
        x86, x64, ppc, i386, amd64, UNDEFINED
    }

    static final OperationSystem os = getOperationSystem();
    static final Architecture arch = getArchitecture();


    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }
    public static boolean isOSX() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }
    public static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }


    private static OperationSystem getOperationSystem() {
        if (isWindows()) return OperationSystem.WINDOWS;
        if (isOSX()) return OperationSystem.OSX;
        if (isLinux()) return OperationSystem.LINUX;
        return OperationSystem.UNDEFINED;
    }
    public static Architecture getArchitecture() {
        try {
            switch(os) {
                case WINDOWS:
                    return getArchWindows();
                case OSX:
                    return getArchOSX();
                case LINUX:
                    return getArchLinux();
                case UNDEFINED:
                    break;
                default:
                    throw new IllegalStateException("Cannot deduce architecture for system: "+os.toString());
            }
        } catch (IOException e) {
            log.error(e.toString());
        }
        return Architecture.UNDEFINED;
    }

    private static Architecture getArchWindows() {
        boolean is64bit = (System.getenv("ProgramFiles(x86)") != null);
        if (is64bit) {
            return Architecture.x64;
        } else {
            return Architecture.x86;
        }
    }

    private static Architecture getArchLinux() throws IOException {
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"uname","-m"};
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));
        String bitsString = stdInput.readLine();
        if (bitsString.contains("64")) return Architecture.amd64;
        else return Architecture.i386;
    }

    private static Architecture getArchOSX() throws IOException {
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"uname", "-m"};
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));
        String bitsString = stdInput.readLine();
        if (bitsString.contains("64")) {
            return Architecture.x64;
        } else if (bitsString.toLowerCase().contains("ppc")) {
            return Architecture.ppc;
        } else {
            return Architecture.i386;
        }
    }

    public static String getJavaExecPathOnWindows() {
        final String javaLibraryPath = System.getProperty("java.library.path");
        final File javaExeFile = new File(
                javaLibraryPath.substring(0, javaLibraryPath.indexOf(';')) + "\\java.exe"
        );
        final String javaExePath =
                javaExeFile.exists() ? javaExeFile.getAbsolutePath() : "java";
        return javaExePath;
    }

    public static String getJavaExecOSX() {
        return System.getProperty("java.home")+ File.separator+"bin/java.exe";
    }
    public static String getJavaExecLinux() {
        return System.getProperty("java.home")+File.separator+"bin/java.exe";
    }

    public static String getJavaExecPath() {
        if (Arch.isWindows()) {
            return getJavaExecPathOnWindows();
        } else if (Arch.isOSX()) {
            return getJavaExecOSX();
        } else if (Arch.isLinux()) {
            return getJavaExecLinux();
        }
        return "NO JAVA FOUND... w8 a minute- how did you run this program then...";
    }

}
