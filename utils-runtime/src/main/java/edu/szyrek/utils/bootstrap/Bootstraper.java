package edu.szyrek.utils.bootstrap;

import edu.szyrek.utils.app.AppFilesLocator;
import edu.szyrek.utils.runtime.Arch;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bebebaba on 2016-07-17.
 */
@Slf4j
public class Bootstraper {
    public final static String JAVA_CLASS_LOADER_PARAM = "java.system.class.loader";
    public final static String JAR_ARGUMENT = "-jar";

    private static String javaExecPath = Arch.getJavaExecPath();
    private static RuntimeMXBean wrapperApp = ManagementFactory.getRuntimeMXBean();

    public static void bootstrap(String[] args, String classLoaderName) throws IOException, URISyntaxException {
        List<String> command = getProcessArgs(classLoaderName);
        Arrays.stream(args).forEach(argument->command.add(argument));
        log.trace("Running: "+command.stream().collect(Collectors.joining(" ")));
        ProcessBuilder run = new ProcessBuilder(command);

        run.redirectInput(ProcessBuilder.Redirect.INHERIT);
        run.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        run.redirectError(ProcessBuilder.Redirect.INHERIT);

        try {
            Process pr = run.start();
            int houstonWeveGotAProblem = pr.waitFor();
            log.trace("Application exited"+ (houstonWeveGotAProblem == 0 ?
                    " correctly" : (" with error code "+houstonWeveGotAProblem)
            ));
        } catch (IOException e) {
            log.error("Failed to run {}", command, e);
        } catch (InterruptedException e) {
            log.error("Interrupted while running {}", command, e);
        }
    }

    private static List<String> getProcessArgs(String classLoaderName) throws IOException, URISyntaxException {
        List<String> res = new LinkedList<>();
        res.add(javaExecPath);
        res.addAll(wrapperApp.getInputArguments());
        // TODO: Hmmm...
        res.add(formatSystemPropertyArg(JAVA_CLASS_LOADER_PARAM, classLoaderName));
        res.add(JAR_ARGUMENT);
        res.add(AppFilesLocator.getJarPath());
        return res;
    }


    public static String formatArg(String name, String value) {
        return "-"+name+" "+value;
    }

    public static String formatArg(String value) {
        return value;
    }

    public static String formatSystemPropertyArg(String name) {
        return "-D"+name;
    }

    public static String formatSystemPropertyArg(String name, String value) {
        return "-D"+name+"="+value;
    }

}
