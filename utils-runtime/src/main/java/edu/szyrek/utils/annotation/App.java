package edu.szyrek.utils.annotation;

import edu.szyrek.utils.runtime.processors.DefaultClassLoader;
/**
 * Created by bebebaba on 2016-07-17.
 */
public @interface App {
    String name();
    boolean mainApp() default false;
    Class<?> classLoader() default DefaultClassLoader.class;
}
