package edu.szyrek.utils.net;

import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.events.EventBus;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.*;
import java.net.BindException;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by bebebaba on 2016-08-14.
 */
@Slf4j
public class ServerUT {
    private static final int TEST_SERVER_PORT = Server.DEFAULT_PORT;
    private static final Integer FIRST_MSG = 4;
    private static final Integer SECOND_MSG = 12;
    private static final Integer THIRD_MSG = 1323;

    private static CompletableFuture<Boolean> firstMsgOK = new CompletableFuture<>();
    private static CompletableFuture<Boolean> secondMsgOK = new CompletableFuture<>();
    private static CompletableFuture<Boolean> thirdMsgOK = new CompletableFuture<>();

    private static Server server1;
    private static Server server2;

    static class TestProtocol implements Protocol<Integer> {
        @Override
        public void serverHandle(Integer message, OutputStream output) {
            if (message.equals(FIRST_MSG)) {
                firstMsgOK.complete(true);
            } else if (message.equals(SECOND_MSG)) {
                secondMsgOK.complete(true);
            } else if (message.equals(THIRD_MSG)) {
                thirdMsgOK.complete(true);
            }
        }

        @Override
        public void clientHandle(Integer message) {

        }

        @Override
        public Integer toMessage(String message) {
            return Integer.valueOf(message);
        }

        @Override
        public String toString(Integer message) {
            return message.toString();
        }
    }

    @Rule
    public ExpectedException expectation = ExpectedException.none();


    @BeforeClass
    public static void setupServers() throws IOException, IDTakenException {
        server1 = new Server(TEST_SERVER_PORT, new TestProtocol());
        server2 = new Server(TEST_SERVER_PORT+1, new TestProtocol());
    }

    @AfterClass
    public static void tearDownServers() {
        server1.tryShutDown(false);
        server2.tryShutDown(false);
        EventBus.MAIN.shutDownGracefully();
    }

    @Test
    public void testMultipleServers() throws IOException, IDTakenException, ExecutionException, InterruptedException {
        Assert.assertTrue(server1.isUP());
        expectation.expect(BindException.class);
        Server server3 = new Server(TEST_SERVER_PORT, new TestProtocol());
    }

    @Test
    public void testNewServer() throws IOException, IDTakenException, ExecutionException, InterruptedException {
        Assert.assertTrue(server1.isUP());
        Assert.assertTrue(server2.isUP());

        Socket socket = new Socket("localhost", server1.getPort());
        Socket socket2 = new Socket("localhost", server1.getPort());
        Socket socket3 = new Socket("localhost", server1.getPort());
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
            writer.write(FIRST_MSG.toString());
        }
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket2.getOutputStream()))) {
            writer.write(SECOND_MSG.toString());
        }
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket3.getOutputStream()))) {
            writer.write(THIRD_MSG.toString());
        }

        Assert.assertTrue(firstMsgOK.get());
        Assert.assertTrue(secondMsgOK.get());
        Assert.assertTrue(thirdMsgOK.get());
    }

}
