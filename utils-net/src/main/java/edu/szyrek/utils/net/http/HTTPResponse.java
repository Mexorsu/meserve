package edu.szyrek.utils.net.http;

import java.io.OutputStream;

/**
 * Created by bebebaba on 2016-08-21.
 */
public class HTTPResponse extends HTTPMessage  {
    private HTTP.ResponseCode responseCode;

    private HTTPResponse(){}

    public static HTTPResponse HTTP_OK(HTTPRequest requesst) {
        HTTPResponse result = new HTTPResponse();
        result.setResponseCode(HTTP.ResponseCode.OK.toHTTPCode());
        result.httpVersion = requesst.httpVersion;
        return result;
    }

    public static HTTPResponse HTTP_NOT_FOUND(HTTPRequest requesst) {
        HTTPResponse result = new HTTPResponse();
        result.setResponseCode(HTTP.ResponseCode.NOT_FOUD.toHTTPCode());
        result.httpVersion = requesst.httpVersion;
        return result;
    }

    private void setResponseCode(int code) {
        this.responseCode = HTTP.ResponseCode.fromInt(code);
        this.setHeader(HTTP.Header.STATUS, String.valueOf(this.responseCode.toHTTPCode()));
    }

    @Override
    public String toString() {
        return "HTTP"+httpVersion+ " response: " + responseCode.toHTTPCode() + " " + responseCode.toString() + "\n" +super.toString();
    }

    @Override
    public boolean writeToStream(OutputStream output) {
        return tryWriteToStrem(getResponseMessage(), output) && tryWriteToStrem(getHeaders(), output) &&
                tryWriteToStrem(getBody(), output) && tryWriteToStrem(HTTP.END_LINE, output);
    }

    public String getResponseMessage() {
        return "HTTP/"+httpVersion+" "+responseCode.toHTTPCode()+ " "+responseCode.toString();
    }

    @Override
    public String getHeaders() {
        setHeader(HTTP.Header.CONTENT_LENGTH, String.valueOf(this.getBody().length()));
        return super.getHeaders();
    }
}
