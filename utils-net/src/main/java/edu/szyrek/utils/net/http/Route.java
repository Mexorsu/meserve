package edu.szyrek.utils.net.http;

import edu.szyrek.utils.annotation.ConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigTypeAdapter;
import org.json.simple.JSONObject;

import javax.annotation.processing.RoundEnvironment;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bebebaba on 2016-08-21.
 */
public class Route implements JSONConfigType {
    public static Route GET(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.GET, pathPattern, routeTo);
    }
    public static Route PUT(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.PUT, pathPattern, routeTo);
    }
    public static Route POST(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.POST, pathPattern, routeTo);
    }
    public static Route CONNECT(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.CONNECT, pathPattern, routeTo);
    }
    public static Route DELETE(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.DELETE, pathPattern, routeTo);
    }
    public static Route HEAD(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.HEAD, pathPattern, routeTo);
    }
    public static Route OPTIONS(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.OPTIONS, pathPattern, routeTo);
    }
    public static Route PATCH(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.PATCH, pathPattern, routeTo);
    }
    public static Route TRACE(String pathPattern, HTTPRequestHandler routeTo) {
        return new Route(HTTP.Method.TRACE, pathPattern, routeTo);
    }

    private HTTP.Method method;
    private Pattern routePattern;
    private HTTPRequestHandler handler;

    public Route() {

    }

    private Route(HTTP.Method method, String routePattern, HTTPRequestHandler routeTo) {
        this.method = method;
        this.handler = routeTo;
        this.routePattern = Pattern.compile(routePattern);
    }

    public HTTPResponse route(HTTPRequest request) {
        return handler.handle(request);
    }

    public boolean shouldRoute(HTTP.Method method, String path) {
        if (!method.equals(this.method)) return false;
        Matcher matcher = routePattern.matcher(path);
        return matcher.matches();
    }

    @Override
    public void fromJSON(JSONObject json) {

    }
}
