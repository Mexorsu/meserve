package edu.szyrek.utils.net;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.adapter.IntegerAdapter;
import edu.szyrek.utils.events.EventBus;
import edu.szyrek.utils.events.EventListener;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

/**
 * Created by bebebaba on 2016-08-14.
 */

@Slf4j
public class ClientConnection implements Runnable, EventListener<SocketEvent> {
    private Protocol protocol;
    private Socket client;
    private BufferedReader reader;
    private ID eventuBusID;
    private static Semaphore mutex = new Semaphore(1);

    private static final long MESSAGE_POOL_INTERVAL = 100;
    @Configurable(key = "simpleHTMLServer.connectionTimeout", adapter = IntegerAdapter.class)
    private static int DEFAULT_CONNECTION_TIMEOUT;

    private int connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

    private long lastMessageReceived;
    private boolean running = false;

    private Thread connectionThread;

    public ClientConnection(ID busID, Protocol protocol, Socket client) throws IOException {
        this.eventuBusID = busID;
        this.client = client;
        this.protocol = protocol;
        EventBus.getByID(eventuBusID).registerListener(this);
        reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
    }

    public void start() {
        this.running = true;
        this.connectionThread = new Thread(this);
        resetTimeout();
        connectionThread.start();
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    private void resetTimeout() {
        lastMessageReceived = System.currentTimeMillis();
    }

    public void tryShutDown(boolean immediate) {
        try {
            shutDown(immediate);
        } catch (IOException |ExecutionException | InterruptedException e) {
            log.error("Error while shutting  down: ", e);
        }
    }

    private void shutDown(boolean immediate) throws ExecutionException, InterruptedException, IOException {
        this.running = false;
        EventBus.getByID(eventuBusID).ungeristerListener(this);
        if(!immediate){
            log.trace("{} shutting down");
        } else {
            log.warn("{} shutting down in immediate mode");
        }
    }

    private void waitOrTimeOut() {
        try {
            if (System.currentTimeMillis() > lastMessageReceived + connectionTimeout) {
                shutDown(false);
            } else {
                Thread.sleep(MESSAGE_POOL_INTERVAL);
            }
        } catch (IOException|InterruptedException | ExecutionException  e) {
            log.error("Error while timing out,",e);
        }
    }

    @Override
    public void run() {
        try {
            while(running) {
                boolean goWait = false;
                String message = null;
                synchronized (mutex) {
                    if (reader.ready()) {
                        resetTimeout();
                        message = reader.readLine();
                        log.info("{} received message {}", this.toString(), message);
                    } else {
                        goWait = true;
                    }
                }
                if (!goWait&&message!=null&&message!="") {
                    protocol.serverHandle(protocol.toMessage(message), client.getOutputStream());
                } else {
                    waitOrTimeOut();
                }
            }
        } catch (IOException e) {
            log.error("Failed to read from socket {}", client, e);
        }
    }

    @Override
    public void handle(SocketEvent event) {

    }
}