package edu.szyrek.utils.net.http;

import com.google.common.io.Files;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.net.Protocol;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.Charset;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by bebebaba on 2016-08-14.
 */
@Slf4j
public class HTTPProtocol implements Protocol<String> {
    private static final Pattern HTMLGET = Pattern.compile("GET ([^ ]+) HTTP/1.1");
    private static final Pattern ENDMESSAGE = Pattern.compile("");
    private static final Pattern SPECIAL = Pattern.compile("bebebeb");

    private HTTPRequest request = new HTTPRequest();
    private Router router = new Router();

    public HTTPProtocol(Router router) {
        this.router = router;
    }

    private void processRequest(HTTPRequest request, OutputStream output) {
        log.info("Built request: {}", request.toString());
        HTTPResponse response = router.route(request);
        log.info("Built response: {}", response.toString());
        boolean result = response.writeToStream(output);
        log.info("{} {} to stream {}",
                result==true ? "Wrote ":"Failed to write ",
                response.toString(),
                output
        );
    }

    @Override
    public void serverHandle(String message, OutputStream output) {
        Matcher getMatcher = HTMLGET.matcher(message);
        request.process(message);
        if (request.isReady()) {
            processRequest(request, output);
        }

//
//        if (getMatcher.matches()){
//            fileToServe = new File(serverRoot.getAbsolutePath()+getMatcher.group(1));
//        } else if (ENDMESSAGE.matcher(message).matches()) {
//            log.info("Received end message: {}", message);
//
//            StringBuffer page = new StringBuffer();
//            try {
//                for (String line: Files.readLines(fileToServe, Charset.defaultCharset())) {
//                    page.append(line+"\n");
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            StringBuffer headers = new StringBuffer();
//            headers.append("HTTP/1.1 200 OK"+ HTTP.END_LINE);
//            headers.append("Content-Length: "+page.length()+ HTTP.END_LINE);
//            headers.append("Content-Type:text/html; charset=utf-8"+ HTTP.END_LINE);
//            boolean writeSucceed = tryWriteToStrem(headers.toString()+HTTP.END_LINE, output) &&
//                tryWriteToStrem(page.toString()+HTTP.END_LINE, output);
//        } else if (SPECIAL.matcher(message).matches()) {
//            tryWriteToStrem("bebebaba", output);
//        }
    }

    @Override
    public void clientHandle(String message) {

    }

    @Override
    public String toMessage(String message) {
        return message;
    }

    @Override
    public String toString(String message) {
        return message;
    }
}
