package edu.szyrek.utils.net;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.adapter.IntegerAdapter;
import edu.szyrek.utils.events.EventBus;
import edu.szyrek.utils.events.EventBusID;
import edu.szyrek.utils.events.EventBusShutDownEvent;
import edu.szyrek.utils.events.EventListener;
import edu.szyrek.utils.random.Random;
import edu.szyrek.utils.random.RandomString;
import edu.szyrek.utils.random.StringBounds;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Created by bebebaba on 2016-08-14.
 */
@Slf4j
public class Server implements Runnable, EventListener<EventBusShutDownEvent> {
    @Configurable(key = "simpleHTMLServer.defaultPort", adapter = IntegerAdapter.class)
    public static Integer DEFAULT_PORT;

    private boolean running = false;
    private ServerSocket socket;
    private String interruptSignal;
    private Protocol protocol;
    private Thread serverThread;
    private ID eventBusID = EventBusID.create();
    private int port;

    @Override
    public void run() {
        while(running) {
            try {
                new ClientConnection(eventBusID, protocol, socket.accept()).start();
            } catch (IOException e) {
                log.error("Server crashed ",e);
                tryShutDown(true);
            }

        }
    }

    public void tryShutDown(boolean immediate) {
        try {
            shutDown(immediate);
        } catch (IOException | InterruptedException | ExecutionException e) {
            log.error("Error caught during shutdown",e);
        }
    }


    private void shutDown(boolean immediate) throws ExecutionException, InterruptedException, IOException {
        EventBus.getByID(eventBusID).shutDown(immediate);
        this.running = false;
        sendShutDownSignal();
        socket.close();
        if(immediate){
            log.warn("Server shut down in immediate mode");
            serverThread.interrupt();
        } else {
            log.trace("Server shut down gracefully");
        }
    }

    public Server(int port, Protocol protocol) throws IOException, IDTakenException {
        this.socket = new ServerSocket(port);
        this.protocol = protocol;
        this.port = port;
        this.running = true;
        this.interruptSignal = Random.get(String.class, StringBounds.bound(20, RandomString.TYPABLE));
        EventBus.registerEventBus(eventBusID);
        EventBus.getByID(eventBusID).registerListener(this);

        serverThread = new Thread(this);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                if (serverThread.isAlive()) {
                    tryShutDown(false);
                }
            }
        });
        serverThread.setDaemon(false);
        serverThread.start();
        log.info("Server listening on "+port);
    }

    public Server(Protocol protocol) throws IOException, IDTakenException {
        this(DEFAULT_PORT, protocol);
    }

    public Server(int port) throws IOException, IDTakenException {
        this(port, new PrintProtocol());
    }

    public Server() throws IOException, IDTakenException {
        this(DEFAULT_PORT, new PrintProtocol());
    }

    public boolean isUP() throws IOException {
        try (Socket clientSocket = new Socket("localhost", port)){
            return clientSocket.isConnected();
        }
    }

    private void sendShutDownSignal() {
        try (Socket clientSocket = new Socket("localhost", port);
             DataOutputStream output = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()))) {
             output.writeUTF(interruptSignal);
        } catch (IOException e) {
            log.error("Failed to send interrupt signal to server",e);
        }
    }

    public int getPort() {
        return port;
    }

    @Override
    public void handle(EventBusShutDownEvent event) {

    }
}
