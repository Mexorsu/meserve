package edu.szyrek.utils.net.http;

import edu.szyrek.utils.annotation.ConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigTypeAdapter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.annotation.processing.RoundEnvironment;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by bebebaba on 2016-08-21.
 */
public class Router implements JSONConfigType {
    public Set<Route> routes = new HashSet<>();
    public Router() {

    }
    public void registerRoute(Route route) {
        this.routes.add(route);
    }
    public HTTPResponse route(HTTPRequest request) {
        for (Route route: routes) {
            if (route.shouldRoute(request.getMethod(), request.getUri().toString())) {
                return route.route(request);
            }
        }
        throw new IllegalStateException("No routes registered to handle "+ request.getUri().toString());
    }

    @Override
    public void fromJSON(JSONObject json) {
        for (Object obj: (JSONArray)json.get("routes")) {
            JSONObject routeJson = (JSONObject) obj;
            Route newRoute = new Route();
            newRoute.fromJSON(routeJson);
            routes.add(newRoute);
        }
    }
}
