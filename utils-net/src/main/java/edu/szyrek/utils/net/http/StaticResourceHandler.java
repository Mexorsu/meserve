package edu.szyrek.utils.net.http;

import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by bebebaba on 2016-08-21.
 */
@Slf4j
public class StaticResourceHandler implements HTTPRequestHandler {
    File resourceRoot;

    public StaticResourceHandler(String resourceRootPath) {
        resourceRoot = new File(resourceRootPath);
        if (!resourceRoot.isDirectory()) {
            throw new IllegalArgumentException("Resource root must be directory, is: "+resourceRootPath);
        }
    }

    @Override
    public HTTPResponse handle(HTTPRequest request) {
        File fileToServe = new File(resourceRoot.getAbsolutePath()+request.getUri().toString());
        if (fileToServe.exists()) {
            HTTPResponse response = HTTPResponse.HTTP_OK(request);
            StringBuffer page = new StringBuffer();
            try {
                for (String line: Files.readLines(fileToServe, Charset.defaultCharset())) {
                    page.append(line+"\n");
                }
            } catch (IOException e) {
                log.error("Failed to read page {}", fileToServe, e);
            }
            response.setHeader(HTTP.Header.CONTENT_TYPE ,"text/html; charset=utf-8");
            response.setBody(page.toString());
            response.setHeader(HTTP.Header.CONTENT_LENGTH ,String.valueOf(page.length()));
            return response;
        } else {
            return HTTPResponse.HTTP_NOT_FOUND(request);
        }
    }
}
