package edu.szyrek.utils.net.http;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.OutputStream;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bebebaba on 2016-08-21.
 */
public class HTTPRequest extends HTTPMessage {
    private static final Pattern HTTPREQUEST = Pattern.compile("("+HTTP.Method.MATCH_ALL()+") ([^ ]+) HTTP/(1.1)");
    private static final Pattern HEADER = Pattern.compile("("+HTTP.Header.MATCH_ALL()+")[ \t]*:[ \t]*(.*)[ \t]*");
    private static final Pattern REQUEST_END = Pattern.compile("^$");

    private HTTP.Method method;
    private URI uri;
    private boolean ready = false;

    @Override
    public String toString() {
        return "HTTP"+httpVersion+ " " + method.toString() + " request: " + uri.toString() + "\n" +super.toString();
    }

    public String getRequestMessage() {
        return this.getMethod().toString()+ " " + this.getUri().toString() + " HTTP/"+getHttpVersion();
    }

    @Override
    public boolean writeToStream(OutputStream output) {
        return tryWriteToStrem(getRequestMessage(), output) && tryWriteToStrem(getHeaders(), output) &&
                tryWriteToStrem(getBody(), output) && tryWriteToStrem(HTTP.END_LINE, output);
    }

    public HTTPRequest() {

    }

    public void process(String msg) {
        Matcher requestMatcher = HTTPREQUEST.matcher(msg);
        Matcher headerMatcher = HEADER.matcher(msg.toLowerCase());
        Matcher requestEndMatcher = REQUEST_END.matcher(msg);

        if (requestMatcher.matches()) {
            this.method = HTTP.Method.valueOf(requestMatcher.group(1));
            this.uri = URI.create(requestMatcher.group(2));
            this.httpVersion = requestMatcher.group(3);
        } else if (headerMatcher.matches()) {
            setHeader(HTTP.Header.fromString(headerMatcher.group(1)), headerMatcher.group(2));
        } else if (requestEndMatcher.matches()) {
            if (validate()) {
                ready = true;
            } else {
                throw new IllegalStateException("Received incomplete request: "+this.toString());
            }
        } else {
            this.appendToBody(msg);
        }
    }

    public boolean isReady() {
        return this.ready;
    }

    private boolean validate() {
        return uri != null &&
                method != null &&
                headers.containsKey(HTTP.Header.HOST);
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public HTTP.Method getMethod() {
        return method;
    }

}
