package edu.szyrek.utils.net;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created by bebebaba on 2016-08-14.
 */
public interface Protocol<T> {
    public void serverHandle(T message, OutputStream output);
    public void clientHandle(T message);
    public T toMessage(String message);
    public String toString(T message);
}
