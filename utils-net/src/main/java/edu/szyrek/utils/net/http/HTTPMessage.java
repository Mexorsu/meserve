package edu.szyrek.utils.net.http;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by bebebaba on 2016-08-21.
 */
@Slf4j
public abstract class HTTPMessage {
    protected Map<HTTP.Header,String> headers = new HashMap<>();
    protected String httpVersion;
    private StringBuffer body = new StringBuffer("");

    public void setHeader(HTTP.Header key, String value) {
        this.headers.put(key, value);
    }

    public void setHeader(String key, String value) {
        this.headers.put(HTTP.Header.fromString(key), value);
    }

    public void setBody(String newBody) {
        this.body = new StringBuffer();
        this.body.append(newBody);
    }

    public void appendToBody(String line) {
        this.body.append(line+"\n");
    }

    public String getBody() {
        return body.toString()+HTTP.END_LINE;
    }

    public String getHeaders() {
        return headers.keySet().stream().map(
                key ->
                        key.toString()+HTTP.HEADER_SEPARATOR +
                                headers.get(key) +
                                HTTP.END_LINE)
                .collect(Collectors.joining())+HTTP.END_LINE;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Headers: \n");
        buffer.append(getHeaders());
        buffer.append("Body: \n");
        buffer.append(body);
        return buffer.toString();
    }

    public abstract boolean writeToStream(OutputStream stream);


    protected boolean tryWriteToStrem(String message, OutputStream stream) {
        try {
            writeToStrem(message, stream);
            log.trace("Written {} to {}", message, stream);
            return true;
        } catch (IOException e) {
            log.error("Failed to write {} to stream {}", message, stream);
            return false;
        }
    }

    protected void writeToStrem(String message, OutputStream stream) throws IOException {
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(stream));
        writer.write(message);
        writer.write(HTTP.END_LINE);
        writer.flush();
    }


    public String getHttpVersion() {
        return httpVersion;
    }
}
