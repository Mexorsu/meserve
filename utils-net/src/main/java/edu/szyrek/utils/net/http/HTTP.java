package edu.szyrek.utils.net.http;

/**
 * Created by bebebaba on 2016-08-21.
 */
public class HTTP {
    public static final String END_LINE = "\r\n";
    public static final String HEADER_SEPARATOR = ": ";

    public static final String CACHE_CONTROL_HEADER_KEY = "cache-control";
    public static final String ACCEPT_ENCODING_HEADER_KEY = "accept-encoding";
    public static final String ACCEPT_LANGUAGE_HEADER_KEY = "accept-language";
    public static final String USER_AGENT_HEADER_KEY = "user-agent";
    public static final String HOST_HEADER_KEY = "host";
    public static final String CONNECTION_HEADER_KEY = "connection";
    public static final String ACCEPT_HEADER_KEY = "accept";
    public static final String REFERER_HEADER_KEY = "referer";
    public static final String UPGRADE_INSECURE_HEADER_KEY = "upgrade-insecure-requests";
    public static final String PRAGMA_HEADER_KEY = "pragma";
    public static final String CONTENT_LENGTH_HEADER_KEY = "content-length";
    public static final String CONTENT_TYPE_HEADER_KEY = "content-type";
    public static final String STATUS_HEADER_KEY = "status";

    public enum ResponseCode {
        OK, NOT_FOUD;

        public int toHTTPCode() {
            switch (this){
                case OK:
                    return 200;
                case NOT_FOUD:
                    return 404;
            }
            throw new IllegalArgumentException("Unhandler response code "+this.toString());
        }
        public static ResponseCode fromInt(int responseCode) {
            switch (responseCode){
                case 200:
                    return OK;
                case 404:
                    return NOT_FOUD;
            }
            throw new IllegalArgumentException("Unhandler response code "+responseCode);
        }

    }

    public enum Method {
        GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, CONNECT, PATCH;
        private static final String REGEX_OR = "|";
        public static String MATCH_ALL() {
            boolean first = true;
            StringBuffer sb = new StringBuffer();
            for(Method method: Method.values()) {
                if (first) {
                    first = false;
                    sb.append(method.toString());
                } else {
                    sb.append(REGEX_OR);
                    sb.append(method.toString());
                }
            }
            return sb.toString();
        }
    }

    public enum Header {
        CACHE_CONTROL, ACCEPT_ENCODING, ACCEPT_LANGUAGE, USER_AGENT, HOST, CONNECTION, ACCEPT, REFERER, UPGRADE_INSECURE, PRAGMA, CONTENT_LENGTH, CONTENT_TYPE, STATUS;
        private static final String REGEX_OR = "|";

        @Override
        public String toString() {
            switch (this) {
                case CACHE_CONTROL:
                    return CACHE_CONTROL_HEADER_KEY;
                case ACCEPT_ENCODING:
                    return ACCEPT_ENCODING_HEADER_KEY;
                case ACCEPT_LANGUAGE:
                    return ACCEPT_LANGUAGE_HEADER_KEY;
                case USER_AGENT:
                    return USER_AGENT_HEADER_KEY;
                case HOST:
                    return HOST_HEADER_KEY;
                case CONNECTION:
                    return CONNECTION_HEADER_KEY;
                case ACCEPT:
                    return ACCEPT_HEADER_KEY;
                case REFERER:
                    return REFERER_HEADER_KEY;
                case UPGRADE_INSECURE:
                    return UPGRADE_INSECURE_HEADER_KEY;
                case PRAGMA:
                    return PRAGMA_HEADER_KEY;
                case CONTENT_LENGTH:
                    return CONTENT_LENGTH_HEADER_KEY;
                case CONTENT_TYPE:
                    return CONTENT_TYPE_HEADER_KEY;
                case STATUS:
                    return STATUS_HEADER_KEY;

            }
            throw new IllegalArgumentException("Unhandled value: "+ this.toString());

        }

        public static Header fromString(String string) {
            switch (string.toLowerCase()) {
                case CACHE_CONTROL_HEADER_KEY:
                    return CACHE_CONTROL;
                case ACCEPT_ENCODING_HEADER_KEY:
                    return ACCEPT_ENCODING;
                case ACCEPT_LANGUAGE_HEADER_KEY:
                    return ACCEPT_LANGUAGE;
                case USER_AGENT_HEADER_KEY:
                    return USER_AGENT;
                case HOST_HEADER_KEY:
                    return HOST;
                case CONNECTION_HEADER_KEY:
                    return CONNECTION;
                case ACCEPT_HEADER_KEY:
                    return ACCEPT;
                case REFERER_HEADER_KEY:
                    return REFERER;
                case UPGRADE_INSECURE_HEADER_KEY:
                    return UPGRADE_INSECURE;
                case PRAGMA_HEADER_KEY:
                    return PRAGMA;
                case CONTENT_LENGTH_HEADER_KEY:
                    return CONTENT_LENGTH;
                case CONTENT_TYPE_HEADER_KEY:
                    return CONTENT_TYPE;
                case STATUS_HEADER_KEY:
                    return STATUS;
            }
            throw new IllegalArgumentException("No such value for HTTPRequest.Header "+string);
        }

        public static String MATCH_ALL() {
            boolean first = true;
            StringBuffer sb = new StringBuffer();
            for(Header method: Header.values()) {
                if (first) {
                    first = false;
                    sb.append(method.toString());
                } else {
                    sb.append(REGEX_OR);
                    sb.append(method.toString());
                }
            }
            return sb.toString();
        }
    }

}
