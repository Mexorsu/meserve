package edu.szyrek.utils.net;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * Created by bebebaba on 2016-08-14.
 */
@Slf4j
public class PrintProtocol implements Protocol<String> {
    @Override
    public void serverHandle(String message, OutputStream output) {
        System.out.println("--* MESSAGE RESEIVED BY SERVER: *--\n\""+message+"\"");
    }

    @Override
    public void clientHandle(String message) {

    }

    @Override
    public String toMessage(String message) {
        return message;
    }

    @Override
    public String toString(String message) {
        return message;
    }
}
