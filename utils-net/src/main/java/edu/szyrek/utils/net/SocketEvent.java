package edu.szyrek.utils.net;

import edu.szyrek.utils.events.Event;

/**
 * Created by bebebaba on 2016-08-14.
 */
public class SocketEvent extends Event {
    private String message;

    public SocketEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
