package edu.szyrek.utils.net.http;

/**
 * Created by bebebaba on 2016-08-21.
 */
public interface HTTPRequestHandler {
    public HTTPResponse handle(HTTPRequest request);
}
