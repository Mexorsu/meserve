package com.example.app;

import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.net.Server;
import edu.szyrek.utils.net.http.HTTPProtocol;
import edu.szyrek.utils.net.http.Route;
import edu.szyrek.utils.net.http.Router;
import edu.szyrek.utils.net.http.StaticResourceHandler;

import java.io.File;
import java.io.IOException;

/**
 * Created by bebebaba on 2016-08-14.
 */
public class ExampleServer extends Server {


    //@Configurable(key = "simpleHTMLServer.router")
    //private static Router routerFromConfig;

    @Configurable(key="simpleHTMLServer.documentRoot")
    private static String DEFAULT_SERVER_ROOT_PATH;

    public ExampleServer() throws IOException, IDTakenException {
        super(new HTTPProtocol(getRouter()));
    }

    public ExampleServer(int port) throws IOException, IDTakenException {
        super(port, new HTTPProtocol(getRouter()));

    }
    //TODO: replace with reading from config file
    private static Router getRouter() {

//        if (routerFromConfig==null) {
//            throw new IllegalStateException("No simpleHTMLServer.router configured");
//        } else {
//            this.router = routerFromConfig;
//        }

        Router router = new Router();
        router.registerRoute(Route.GET(".*", new StaticResourceHandler(DEFAULT_SERVER_ROOT_PATH)));
        return router;
    }

}
