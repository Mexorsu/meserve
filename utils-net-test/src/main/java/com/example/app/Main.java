package com.example.app;

import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.annotation.App;
import edu.szyrek.utils.meta.ConfigurableClassLoader;
import edu.szyrek.utils.net.Server;

import java.io.IOException;

/**
 * Created by bebebaba on 2016-08-14.
 */
@App(name = "HTTPServer", mainApp = true, classLoader = ConfigurableClassLoader.class)
public class Main {
    public static void main(String[] args) throws IOException, IDTakenException {
        ExampleServer server = new ExampleServer();
    }
}
