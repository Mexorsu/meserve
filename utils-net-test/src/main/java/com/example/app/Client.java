package com.example.app;

import edu.szyrek.utils.annotation.App;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.adapter.IntegerAdapter;
import edu.szyrek.utils.meta.ConfigurableClassLoader;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.Semaphore;

/**
 * Created by bebebaba on 2016-08-21.
 */
@Slf4j
@App(name = "Client", classLoader = ConfigurableClassLoader.class)
public class Client {
    @Configurable(key = "defaultPort", adapter = IntegerAdapter.class)
    private static int port;
    private static Semaphore mutex = new Semaphore(1);
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress(port));
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        write(socket);
        socket.close();
    }


    private static void lock() throws InterruptedException {
        mutex.acquire();
        log.info("Mutex locked");
    }

    private static void unlock() throws InterruptedException {
        mutex.release();
        log.info("Mutex unlocked");
    }

    private static void write(Socket socket){
        try {
            synchronized (mutex) {
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
                writer.write("bebebeb");
                writer.flush();
            }
        } catch (IOException e) {
            log.error("Write fucked",e);
        }
    }


}
