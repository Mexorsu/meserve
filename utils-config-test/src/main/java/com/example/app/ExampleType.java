package com.example.app;

import edu.szyrek.utils.annotation.ConfigType;

/**
 * Created by bebebaba on 2016-07-16.
 */
@ConfigType
public class ExampleType {
    public ExampleType(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public String name;
    public String surname;
    @Override
    public String toString() {
        return "Person: "+name+" "+surname;
    }
}
