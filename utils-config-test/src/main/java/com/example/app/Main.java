package com.example.app;

import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.Configuration;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bebebaba on 2016-07-31.
 */
@Slf4j
public class Main {

    @Configurable(key="customType", adapter = ExampleTypeAdapter.class)
    private static ExampleType valueFromConfig;
    @Configurable(key="myNewVal")
    private static String updatedValue;

    public static void main(String args[]) {
        assert(valueFromConfig.toString().equals("Person: Szymon Zyrek"));
        assert(updatedValue.equals("dupa"));
        Configuration.MAIN().set(Configuration.DEFAULT_CONFIG_ID, "myNewVal", "kupatrupa");
        assert(updatedValue.equals("kupatrupa"));
        System.out.println("Hello from utils-config-test");
    }
}
