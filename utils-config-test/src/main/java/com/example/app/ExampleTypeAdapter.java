package com.example.app;

import edu.szyrek.utils.config.adapter.ITypeAdapter;
import edu.szyrek.utils.config.adapter.TypeAdapterException;

/**
 * Created by bebebaba on 2016-07-16.
 */
@edu.szyrek.utils.annotation.TypeAdapter(from = String.class, to = ExampleType.class)
public class ExampleTypeAdapter <FROM> implements ITypeAdapter<FROM, ExampleType> {
    @Override
    public ExampleType adapt(FROM valueFromFile, Class<? extends ExampleType> targetClass) throws AdaptationException {
        String[] split = valueFromFile.toString().split(",");
        if (split.length != 2) throw new TypeAdapterException(valueFromFile+" is not of ExampleType- should be two strings separated by comma");
        return new ExampleType(split[0], split[1]);
    }
}
