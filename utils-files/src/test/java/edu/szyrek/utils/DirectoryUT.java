package edu.szyrek.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DirectoryUT {
    private static final String GRANDPA_PATH = "/grandpa";
    private static final String PARENT_PATH = "/grandpa/parent";
    private static final String CHILD_PATH = "/grandpa/child";
    private static final String GRANDPA_FILE_PATH = "/grandpa/grandpa.file";;
    private static final String PARENT_FILE_PATH = "/grandpa/parent/parent.file";;
    private static final String CHILD_FILE_PATH = "/grandpa/child/child.file";;

    private static Path tmpDirPath;
    private static Path root;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setup() throws FileNotFoundException {
        ClassLoader cl = FileUT.class.getClassLoader();
        String properResourcePath;
        if(System.getProperty("os.name").toLowerCase().contains("win")) {
            properResourcePath = cl.getResource(".").toString().substring(6);
        } else {
            properResourcePath = cl.getResource(".").toString().substring(5);
        }
        root = Paths.get(properResourcePath);
        tmpDirPath = Paths.get(properResourcePath+File.SEPARATOR+"tmp_dir");
    }

    @After
    public void clearTmp() throws IOException {
        Files.deleteIfExists(tmpDirPath);
    }

    @Test
    public void testOpen() throws FileNotFoundException, NotDirectoryException {
        Assert.assertTrue(new java.io.File(root.toString()).exists());
        Directory.open(root.toString());
        expected.expect(NotDirectoryException.class);
        Directory.open(root.toString()+GRANDPA_FILE_PATH);
        expected = ExpectedException.none();
    }

    @Test
    public void testCreate() throws FileAlreadyExistsException {
        Assert.assertFalse(new java.io.File(tmpDirPath.toString()).exists());
        Directory.create(tmpDirPath.toString());
        Assert.assertTrue(new java.io.File(tmpDirPath.toString()).exists());
        expected.expect(FileAlreadyExistsException.class);
        Directory.create(tmpDirPath.toString());
        expected = ExpectedException.none();
    }

    @Test
    public void testOpenOrCreate() throws NotDirectoryException {
        Assert.assertFalse(new java.io.File(tmpDirPath.toString()).exists());
        Directory.openOrCreate(tmpDirPath.toString());
        Assert.assertTrue(new java.io.File(tmpDirPath.toString()).exists());
        Directory.openOrCreate(tmpDirPath.toString());
    }
}
