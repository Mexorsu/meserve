package edu.szyrek.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class FileUT {
    private final static String TEST_FILE_NAME = "TestFile1";
    private final static String TEST_FILE_EXTENSION = "txt";
    private static String testTextFilePath;
    private static File testFile;

    private static final void assertPathsEqualIgnoringPathSeparators(String expected, String actual){
        String standarisedExpected = expected.replace("\\","/");
        String standarisedActual = actual.replace("\\","/");
        Assert.assertEquals(standarisedExpected, standarisedActual);
    }

    private final static String[] fileLines = new String[] {
        "ala ma kota",
        "kot ma ale",
        "i takie tam"
    };

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setup() throws FileNotFoundException {
        ClassLoader cl = FileUT.class.getClassLoader();
        if(System.getProperty("os.name").toLowerCase().contains("win")) {
            testTextFilePath = cl.getResource(TEST_FILE_NAME + '.' + TEST_FILE_EXTENSION).toString().substring(6);
        } else {
            testTextFilePath = cl.getResource(TEST_FILE_NAME + '.' + TEST_FILE_EXTENSION).toString().substring(5);
        }
        testFile = File.open(testTextFilePath);
    }

    @Test
    public void testFileOpen() throws FileNotFoundException {
        File testFile2 = File.open(testTextFilePath);
        Assert.assertNotNull(testFile2);
    }

    @Test
    public void testGetWrapped() {
        java.io.File rawFile = new java.io.File(testTextFilePath);
        Assert.assertEquals(rawFile, testFile.getWrapped());
    }

    @Test
    public void testFileExists() throws IOException {
        java.io.File rawFile = new java.io.File(testTextFilePath);
        Assert.assertTrue(rawFile.exists());
        Assert.assertTrue(testFile.exists());
        Path tempPath = Paths.get(testTextFilePath+File.TMP_EXT);
        Files.copy(testFile.getPath(), tempPath);
        File tempFile = File.open(tempPath.toString());
        Assert.assertTrue(tempFile.exists());
        tempFile.delete();
        Assert.assertFalse(tempFile.exists());
    }

    @Test
    public void testCreateOrOpen() throws IOException {
        Path tempPath = Paths.get(testTextFilePath+File.TMP_EXT);
        Assert.assertFalse(new java.io.File(tempPath.toString()).exists());
        File tempFile = File.createOrOpen(tempPath.toString());
        Assert.assertTrue(tempFile.exists());
        File tempFile2 = File.createOrOpen(tempPath.toString());
        Assert.assertTrue(tempFile2.exists());
        Files.delete(tempFile2.getPath());
        Assert.assertFalse(tempFile2.exists());
        Assert.assertFalse(tempFile.exists());
    }

    @Test
    public void testCreateOrRecreate() throws IOException {
        Path tempPath = Paths.get(testTextFilePath+File.TMP_EXT);
        Assert.assertFalse(new java.io.File(tempPath.toString()).exists());
        File tempFile = File.createOrRecreate(tempPath.toString());
        Assert.assertTrue(tempFile.exists());
        File tempFile2 = File.createOrRecreate(tempPath.toString());
        Assert.assertTrue(tempFile2.exists());
        Files.delete(tempFile2.getPath());
        Assert.assertFalse(tempFile2.exists());
        Assert.assertFalse(tempFile.exists());
    }

    @Test
    public void testCopy() throws IOException, FileAlreadyExistsException {
        Path tempPath = Paths.get(testTextFilePath+File.TMP_EXT);
        Assert.assertFalse(new java.io.File(tempPath.toString()).exists());
        testFile.copy(tempPath.toString(), true);
        testFile.copy(tempPath.toString(), true);
        expected.expect(FileAlreadyExistsException.class);
        testFile.copy(tempPath.toString(), false);
        expected = ExpectedException.none();
        java.io.File rawFile = new java.io.File(tempPath.toString());
        Assert.assertTrue(rawFile.exists());
        Files.delete(tempPath);
        Assert.assertFalse(rawFile.exists());
        testFile.copy(tempPath.toString(), false);
        Files.delete(tempPath);
    }


    @Test
    public void testCreate() throws FileAlreadyExistsException, IOException {
        Path tempPath = Paths.get(testTextFilePath+File.TMP_EXT);
        Assert.assertFalse(new java.io.File(tempPath.toString()).exists());
        File tempFile = File.create(tempPath.toString());
        Assert.assertTrue(tempFile.exists());
    }

    @Test(expected = FileAlreadyExistsException.class)
    public void testCreateFail() throws FileAlreadyExistsException, IOException {
        File.create(testFile.getPath().toString());
    }

    @Test
    public void testTestSetup() throws FileNotFoundException {

    }

    @Test
    public void testGetPath() {
        Assert.assertEquals(testFile.getPath(), Paths.get(testTextFilePath));
    }

    @Test
    public void testGetParentDir() {
        // Intentionally using "/" instead of File.separator, as on windows gay getResource returns it instead of proper windows spearator
        Path parentDirPath = Paths.get(testTextFilePath.substring(0, testTextFilePath.lastIndexOf("/")));
        Assert.assertEquals(testFile.getParentDir().getPath().toString(), parentDirPath.toString());
    }

    @Test
    public void testBackup() throws IOException {
        Path backupFilePath = testFile.backup();
        File backUpFile = File.open(backupFilePath.toString());
        assertPathsEqualIgnoringPathSeparators(testTextFilePath+File.BACKUP_EXT, backupFilePath.toString());
        Assert.assertTrue(new java.io.File(testTextFilePath+File.BACKUP_EXT).exists());
        try (FileInputStream testFIS = new FileInputStream(testFile.getWrapped()); FileInputStream backupFIS = new FileInputStream(backUpFile.getWrapped())) {
            BufferedReader testFileReader = new BufferedReader(new InputStreamReader(testFIS));
            BufferedReader backupFileReader = new BufferedReader(new InputStreamReader(backupFIS));
            while (testFileReader.ready()&&backupFileReader.ready()) {
                Assert.assertEquals(testFileReader.readLine(), backupFileReader.readLine());
            }
            Assert.assertFalse(testFileReader.ready());
            Assert.assertFalse(backupFileReader.ready());
        }
        Files.delete(backUpFile.getPath());
    }

    @Test
    public void testGetTmpCopy() throws IOException {
        File tmpCopy = testFile.getTmpCopy();
        assertPathsEqualIgnoringPathSeparators(testTextFilePath+File.TMP_EXT, tmpCopy.getPath().toString());
        Assert.assertTrue(tmpCopy.exists());
        try (FileInputStream testFIS = new FileInputStream(testFile.getWrapped()); FileInputStream tmpFIS = new FileInputStream(tmpCopy.getWrapped())) {
            BufferedReader testFileReader = new BufferedReader(new InputStreamReader(testFIS));
            BufferedReader tmpFileReader = new BufferedReader(new InputStreamReader(tmpFIS));
            while (testFileReader.ready()&&tmpFileReader.ready()) {
                Assert.assertEquals(testFileReader.readLine(), tmpFileReader.readLine());
            }
            Assert.assertFalse(testFileReader.ready());
            Assert.assertFalse(tmpFileReader.ready());
        }
        tmpCopy.delete();
    }
    @Test
    public void testReloadFromBackup() throws IOException {
        File tmpCopy = testFile.getTmpCopy();
        testFile.backup();
        testFile.delete();
        Assert.assertFalse(testFile.exists());
        testFile.reloadFromBackup();
        Assert.assertTrue(testFile.exists());
        try (FileInputStream testFIS = new FileInputStream(testFile.getWrapped()); FileInputStream tmpFIS = new FileInputStream(tmpCopy.getWrapped())) {
            BufferedReader testFileReader = new BufferedReader(new InputStreamReader(testFIS));
            BufferedReader tmpFileReader = new BufferedReader(new InputStreamReader(tmpFIS));
            while (testFileReader.ready()&&tmpFileReader.ready()) {
                Assert.assertEquals(testFileReader.readLine(), tmpFileReader.readLine());
            }
            Assert.assertFalse(testFileReader.ready());
            Assert.assertFalse(tmpFileReader.ready());
        }
        tmpCopy.delete();
    }

    @Test
    public void testDelete() throws IOException {
        File tmpCopy = testFile.getTmpCopy();
        Assert.assertTrue(tmpCopy.exists());
        tmpCopy.delete();
        Assert.assertFalse(tmpCopy.exists());
    }
    
    @Test
    public void testGetName() {
        Assert.assertEquals(testFile.getName(), TEST_FILE_NAME);
    }

    @Test
    public void testGetLastPathComponent() {
        Assert.assertEquals(testFile.getLastPathCopmonent(), TEST_FILE_NAME+'.'+TEST_FILE_EXTENSION);
    }

    @Test
    public void testGetExtension() {
        Assert.assertEquals(testFile.getExtension(), TEST_FILE_EXTENSION);
    }

    @Test
    public void testGetInputStream() throws IOException {
        try (FileInputStream fis = testFile.getInputStream()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            int line = 0;
            while(reader.ready()) {
                Assert.assertEquals(fileLines[line++], reader.readLine());
            } 
        }
    }
    @Test
    public void testGetOutputStream() throws IOException {
        Path newFilePath = Paths.get(testFile.getPath().toString()+'.'+File.TMP_EXT);
        File newFile = File.create(newFilePath.toString());
        try (FileOutputStream fos = newFile.getOutputStream()) {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
            for(String line: Arrays.asList(fileLines)) {
                writer.write(line);
                writer.newLine();
            }
        }
        try (FileInputStream fis = new FileInputStream(new java.io.File(newFilePath.toString()))) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            int line = 0;
            while(reader.ready()) {
                Assert.assertEquals(fileLines[line++], reader.readLine());
            } 
        }
        newFile.delete();
    }
}
