package edu.szyrek.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class File {
    public static final String SEPARATOR = java.io.File.separator;
    public static final String BACKUP_EXT = ".bak";
    public static final String TMP_EXT = ".tmp";
    private java.io.File wrapped;

    public String getName() {
        return wrapped.getName().replace("."+getExtension(),"");
    }
    public String getLastPathCopmonent() {
        return wrapped.getName();
    }

    @Override
    public String toString() {
        return this.getPath().toString();
    }

    private File(java.io.File underlying) {
        this.wrapped = underlying.getAbsoluteFile();
    }
    public static File open(String path) throws FileNotFoundException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (!f.exists()){
            throw new FileNotFoundException("File "+path+" does not exist");
        }else if (f.isDirectory()) {
            throw new IllegalStateException(path+" is directory");
        }
        return new File(f); 
    }
    public static File create(String path) throws FileAlreadyExistsException, IOException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (f.exists()){
            throw new FileAlreadyExistsException("File "+path+" already exist");
        }
        f.createNewFile();
        return new File(f); 
    }
    public static File createOrRecreate(String path) throws FileAlreadyExistsException, IOException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (f.exists()){
            f.delete();
        }
        f.createNewFile();
        return new File(f);
    }
    public static File createOrOpen(String path) throws IOException {
        java.io.File f = new java.io.File(path).getAbsoluteFile();
        if (!f.exists()){
            f.createNewFile();
        }
        return new File(f); 
    }
    public String getExtension() {
        int i = wrapped.getPath().lastIndexOf('.');
        if (i>0) {
            return wrapped.getPath().substring(i+1);
        }
        return "";
    }
    public FileInputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream(this.wrapped);
    }
    public FileOutputStream getOutputStream() throws FileNotFoundException {
        return new FileOutputStream(this.wrapped);
    }
    public void delete() throws IOException {
        Files.delete(Paths.get(this.wrapped.getAbsolutePath()));
    }

    public File copy(String target, boolean overwrite) throws FileAlreadyExistsException, IOException {
        Path targetPath = Paths.get(target);
        if (overwrite) {
            Files.copy(this.getPath(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        } else {
            Files.copy(this.getPath(), targetPath);
        }
        return File.open(targetPath.toString());
    }

    public File getTmpCopy() throws IOException {
        File tmpFile = File.createOrOpen(this.getParentDir().getPath()+File.SEPARATOR+this.getName()+'.'+this.getExtension()+TMP_EXT);
        Path source = this.getPath();
        Path dest = tmpFile.getPath();
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        return tmpFile;
    }

    public Path backup() throws IOException {
        String originalFilePath = wrapped.getAbsolutePath();
        String extension = getExtension();
        String backupFilePath = originalFilePath.replace(extension,extension+BACKUP_EXT);
        Path source = Paths.get(originalFilePath);
        Path dest = Paths.get(backupFilePath);
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
        return dest;
    }

    public void reloadFromBackup() throws IOException {
        String originalFilePath = wrapped.getAbsolutePath();
        String extension = getExtension();
        String backupFilePath = originalFilePath.replace(extension,extension+BACKUP_EXT);
        Path dest = Paths.get(originalFilePath);
        Path source = Paths.get(backupFilePath);
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
    }

    public Directory getParentDir() {
        try {
            return Directory.open(wrapped.getParent());
        } catch(FileNotFoundException|NotDirectoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Path getPath() {
        return Paths.get(wrapped.getAbsolutePath());
    }

    public java.io.File getWrapped() {
        return wrapped;
    }

    public boolean exists() {
        return wrapped.exists();
    }
    
	public String getDescription() {
		return "File: ["+this.getPath()+"]";
	}
}
