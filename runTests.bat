@echo off
cp %~dp0\test.conf %HOMEDRIVE%%HOMEPATH%\main.conf
echo "---------- Running utils-config-test ----------"
java -Djava.system.class.loader=edu.szyrek.utils.meta.ConfigurableClassLoader -jar utils-config-test\target\utils-config-test-0.0.1-SNAPSHOT-jar-with-dependencies.jar > out.1.my 2>&1
FC out.1 out.1.my | findstr "no difference" | wc -l >  temp.file
set /p VAR=<temp.file
IF %VAR% GTR 0 (echo "* PASSED") ELSE (
	echo "* FAILED"
	echo "Expected: "
	cat out.1
	echo "But was: "
	cat out.1.my
	goto :error
)

echo "---------- Running utils-events-test ----------"
java -jar utils-events-test\target\utils-events-test-0.0.1-SNAPSHOT-jar-with-dependencies.jar > out.2.my 2>&1
FC out.2 out.2.my | findstr "no difference" | wc -l >  temp.file
set /p VAR=<temp.file
IF %VAR% GTR 0 (echo "* PASSED") ELSE (
	echo "* FAILED"
	echo "Expected: "
	cat out.2
	echo "But was: "
	cat out.2.my
	goto :error
)

echo "---------- Running utils-runtime-test" ---------"
java -jar utils-runtime-test\target\utils-runtime-test-0.0.1-SNAPSHOT-jar-with-dependencies.jar SecondApp > out.3.my 2>&1
FC out.3 out.3.my | findstr "no difference" | wc -l >  temp.file
set /p VAR=<temp.file
IF %VAR% GTR 0 (echo "* PASSED") ELSE (
	echo "* FAILED"
	echo "Expected: "
	cat out.3
	echo "But was: "
	cat out.3.my
	goto :error
)

echo "------------- Running utils-test -------------"
java -jar utils-test\target\utils-test-0.0.1-SNAPSHOT-jar-with-dependencies.jar > out.4.my 2>&1
FC out.4 out.4.my | findstr "no difference" | wc -l >  temp.file
set /p VAR=<temp.file
IF %VAR% GTR 0 (echo "* PASSED") ELSE (
	echo "* FAILED"
	echo "Expected: "
	cat out.4
	echo "But was: "
	cat out.4.my
	goto :error
)

goto :success

:error
	echo "-----------------  TEST FAILED  -----------------"
	goto :end
:success
	echo "-----------------  TEST SUCCESS  -----------------"
	goto :end
:end
rem -Xrunjdwp:transport=dt_socket,address=5005,server=y,suspend=y