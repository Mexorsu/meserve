package edu.szyrek.utils;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BoolParserUT {
    public static final String[] validBoolsYes = new String[]{"YES", "Yes", "y", "Y", "true", "TRUE", "tRuE", "1"};
    public static final String[] validBoolsNo = new String[]{"NO", "no", "n", "N", "false", "FaLsE", "0"};
    public static final String[] invalidBool =  new String[]{"YEah", "nope", "a 2fe 2 fe,", "43", "11", "007", "Conan the barbarian"};

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Test
    public void testTrue() {
        for (String s: validBoolsYes) {
            Assert.assertTrue(BoolParser.parseBool(s));
        }
    }

    @Test
    public void testFalse() {
        for (String s: validBoolsNo) {
            Assert.assertFalse(BoolParser.parseBool(s));
        }
    }

    @Test
    public void testFail() {
        for (String s: invalidBool) {
            expected.expect(IllegalArgumentException.class);
            BoolParser.parseBool(s);
        }
    }
}
