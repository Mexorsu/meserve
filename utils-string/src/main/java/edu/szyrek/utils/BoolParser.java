package edu.szyrek.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
@Slf4j
public class BoolParser {
    private static final List<String> BOOL_YES = Arrays.asList(new String[]{"yes", "y", "true", "t", "1"});
    private static final List<String> BOOL_NO = Arrays.asList(new String[]{"no", "n", "false", "f", "0"});
    public static Boolean parseBool(String string) {
        for (String s: BOOL_YES) {
            if (s.equalsIgnoreCase(string)) {
                return true;
            }
        }
        for (String s: BOOL_NO) {
            if (s.equalsIgnoreCase(string)) {
                return false;
            }
        }
        throw new IllegalArgumentException("String "+string+" is not a valid boolean");
    }
}
