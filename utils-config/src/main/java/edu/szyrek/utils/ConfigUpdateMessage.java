package edu.szyrek.utils;

/**
 * Created by bebebaba on 2016-08-02.
 */
public class ConfigUpdateMessage<T> {
    private String configID;
    private String key;
    private T value;

    public ConfigUpdateMessage(String configID, String key, T val) {
        this.configID = configID;
        this.key = key;
        this.value = val;
    }

    public String getConfigID() {
        return configID;
    }

    public void setConfigID(String configID) {
        this.configID = configID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}