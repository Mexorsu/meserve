package edu.szyrek.utils.exception;

public class NoSuchPropertyException extends Exception {
    static final long serialVersionUID = 147236335;
    public  NoSuchPropertyException(String msg) {
        super(msg);
    }
}
