package edu.szyrek.utils.exception;

/**
 * Created by bebebaba on 2016-07-30.
 */
public class BootstrappingException extends Exception {
    public BootstrappingException(String s) {
        super(s);
    }
}
