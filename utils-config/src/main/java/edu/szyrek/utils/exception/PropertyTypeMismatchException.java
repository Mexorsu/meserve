package edu.szyrek.utils.exception;

public class PropertyTypeMismatchException extends Exception {
    static final long serialVersionUID = 147236334;
    public  PropertyTypeMismatchException(String msg) {
        super(msg);
    }
}
