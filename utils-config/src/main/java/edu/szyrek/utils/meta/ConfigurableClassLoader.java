package edu.szyrek.utils.meta;

import edu.szyrek.utils.ConfigUpdateMessage;
import edu.szyrek.utils.annotation.ConfigType;
import edu.szyrek.utils.config.Configuration;
import edu.szyrek.utils.config.adapter.AdapterRegistry;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.adapter.ITypeAdapter;
import edu.szyrek.utils.config.adapter.JSONConfigType;
import edu.szyrek.utils.config.adapter.StringAdapter;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sun.reflect.CallerSensitive;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bebebaba on 2016-07-16.
 */
@Slf4j
public class ConfigurableClassLoader extends MetaClassLoader {
    private Set<String> typeAdapterNames = new HashSet<>();
    private Set<String> specialTypes = new HashSet<>();
    private Map<FullKey, Collection<Field>> configuredFields = new HashMap<>();
    private static ConfigurableClassLoader instance;

    public ConfigurableClassLoader(ClassLoader parent) {
        super(parent);
    }

    public ConfigurableClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    @Override
    protected synchronized void initialize() {
        log.trace("Initializing "+this.getClass().getCanonicalName());
        initSpecialTypes();
        initTypes();
        initAdapters();
    }

    @Override
    protected Class<?> reallyLoad(String className) throws ClassNotFoundException {
        log.trace("Loading Class '{}'", className);
        instance = this;

        if (className.equals("com.example.app.Russian")){
          return handleDrunkenRussians(className);
        } else if (isSpecialClass(className)) {
            return loadSpecialClass(className);
        } else {
            return loadNormalClass(className);
        }

    }

    private boolean isSpecialClass(String className) {
        return specialTypes.contains(className);
    }

    private Class<?> loadSpecialClass(String className) throws ClassNotFoundException {
        Class<?> result = null;
        log.trace("Overriding classloader for " + className);
        result = parent.loadClass(className);
        return result;
    }

    private Class<?> handleDrunkenRussians(String className) throws ClassNotFoundException {
        Class<?> parent = loadNormalClass(className);
        Class<?> proxy = loadNormalClass("com.example.app.DrunkenRussianProxy");
        return proxy;
    }

    private Class<?> loadNormalClass(String className) throws ClassNotFoundException {
        Class<?> result = null;
        try {
            // try myself
            result = super.loadClass(className, true);
        } catch (ClassNotFoundException e) {
            log.warn("Can't load {}, falling back to {}", className, parent.toString(), e);
            // cry for daddy
            result = parent.loadClass(className);
        }
        configureClass(result);
        return result;
    }


    public void configureClass(Class<?> clazz) {
        for (Field field: clazz.getDeclaredFields()) {
            if (field.getDeclaredAnnotations().length>0&&field.isAnnotationPresent(Configurable.class)) {
                Configurable configurable = field.getAnnotation(Configurable.class);
                tryHandleField(field, configurable, clazz);
                saveToConfigured(configurable, field);
            }
        }
    }

    private void initSpecialTypes() {
        specialTypes.add(ConfigurableClassLoader.class.getCanonicalName());
        specialTypes.add(FullKey.class.getCanonicalName());
        specialTypes.add(Configurable.class.getCanonicalName());
        specialTypes.add(ConfigUpdateMessage.class.getCanonicalName());
        specialTypes.add(JSONConfigType.class.getCanonicalName());
        specialTypes.add(JSONObject.class.getCanonicalName());
    }

    private void initTypes() {
        if (Configuration.MAIN().isSet(Configuration.META_CONFIG_ID, Configuration.CONFIG_TYPES)) {
            try {
                JSONArray value = Configuration.MAIN().get(Configuration.META_CONFIG_ID, Configuration.CONFIG_TYPES);
                for (Object specialType: value) {
                    JSONObject jsonNode = (JSONObject) specialType;
                    String quotedString = (String) jsonNode.get("class");
                    Pattern regex = Pattern.compile(".*\"([^\"]*)\".*");
                    Matcher matcher = regex.matcher(quotedString);
                    String unquotedString = quotedString;
                    while(matcher.find()) {
                        unquotedString = matcher.group(1);//regex.matcher(quotedString).toMatchResult().group(1);
                    }
                    specialTypes.add(unquotedString);
                }
            } catch (NoSuchPropertyException e) {
                log.error("Failed to init types ",e);
            }
        }
    }

    private void initAdapters() {
        if (Configuration.MAIN().isSet(Configuration.META_CONFIG_ID, Configuration.TYPE_ADAPTERS)) {
            JSONArray value = null;
            try {
                value = Configuration.MAIN().get(Configuration.META_CONFIG_ID, Configuration.TYPE_ADAPTERS);
            } catch (NoSuchPropertyException e) {
                log.error("Luke i am your father");
            }
            for (Object typeAdapter: value) {
                    JSONObject jsonNode = (JSONObject) typeAdapter;
                    String quotedString = (String) jsonNode.get("class");
                    Pattern regex = Pattern.compile(".*\"([^\"]*)\".*");
                    Matcher matcher = regex.matcher(quotedString);
                    String unquotedString = quotedString;
                    while(matcher.find()) {
                        unquotedString = matcher.group(1);//regex.matcher(quotedString).toMatchResult().group(1);
                    }
                    typeAdapterNames.add(unquotedString);
                    specialTypes.add(unquotedString);
                    log.trace("Loader adapter {}", unquotedString);
            }

            AdapterRegistry.loadAdapters(typeAdapterNames);
        }
    }

    private void tryHandleField(Field field, Configurable configurable, Class<?> clazz) {
        try {
            handleField(field, configurable, clazz);
        } catch (ITypeAdapter.AdaptationException | InstantiationException | ClassNotFoundException | NoSuchPropertyException | IllegalAccessException  e) {
            log.error("Failed to configure field {}",field.toString(), e);
        }
    }

    private void saveToConfigured(Configurable configurable, Field field) {
        FullKey key = new FullKey(configurable.configID(), configurable.key());
        if (!configuredFields.containsKey(key)) {
            configuredFields.put(key, new HashSet<>());
        }
        configuredFields.get(key).add(field);
    }

    private void handleField(Field field, Configurable configurable, Class<?> clazz) throws IllegalAccessException, NoSuchPropertyException, ClassNotFoundException, InstantiationException, ITypeAdapter.AdaptationException {
        Object value = Configuration.MAIN().get(configurable.configID(), configurable.key());
        String adapterName = getAdapterName(field, configurable);
        Object adaptedValue = adapt(value, field.getType(), adapterName);
        trySetFieldValue(field, adaptedValue);
    }

    private String getAdapterName(Field field, Configurable configurable){
        if (hasCustomAdapterSetInConfigurable(configurable)) {
            return configurable.adapter().getCanonicalName();
        } else if (hasCustomAdapterSetInConfigType(field.getType())) {
            return getAdapterNameFromConfigTypeAnnotation(field.getType());
        } else {
            return StringAdapter.class.getCanonicalName();
        }
    }

    private boolean hasCustomAdapterSetInConfigurable(Configurable configurable) {
        return !configurable.adapter().getCanonicalName().equals(StringAdapter.class.getCanonicalName());
    }

    private boolean hasCustomAdapterSetInConfigType(Class<?> configType) {
        return !getAdapterNameFromConfigTypeAnnotation(configType).equals(StringAdapter.class.getCanonicalName());
    }

    private Object adapt(Object value, Class targetClass, String adapterName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, ITypeAdapter.AdaptationException {
        ITypeAdapter adapter = null;
        if (!AdapterRegistry.hasAdapterForName(adapterName)) {
            AdapterRegistry.loadAdapter(adapterName);
        }
        adapter = AdapterRegistry.getAdapterForName(adapterName);

        return adapter.adapt(value, targetClass);
    }

    private static String getAdapterNameFromConfigTypeAnnotation(Class<?> clazz) {
        try {
            for (Annotation annotation : clazz.getDeclaredAnnotations()) {
                if (annotation.annotationType().getCanonicalName().equals(ConfigType.class.getCanonicalName())) {
                    return ((Class<?>) annotation.getClass().getDeclaredMethod("adapter").invoke(annotation)).getCanonicalName();
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.error("Failed to instantiate adapter for {}", clazz.getCanonicalName(), e);
        }
        return StringAdapter.class.getCanonicalName();
    }

    private void trySetFieldValue(Field field, Object value) {
        try {
            setFieldValue(field, value);
        } catch (IllegalAccessException e) {
            log.error("Failed to set {} to {}", field.toString(), value.toString(), e);
        }
    }

    private void setFieldValue(Field field, Object value) throws IllegalAccessException {
        log.trace("Setting field {} to value {}", field.toString(), value.toString());
        field.setAccessible(true);
        field.set(null, value);
        field.setAccessible(false);
    }

    public static void notifyConfigChanged(ConfigUpdateMessage change) {
        if (instance!=null) {
            instance.propagateConfigChange(change);
        } else {
            log.warn("Configuration changed before ConfigurableClassLoader got initialized! {}:{} set to {}", change.getConfigID(), change.getKey(), change.getValue());
        }
    }

    public void propagateConfigChange(ConfigUpdateMessage change) {
        FullKey configKey = new FullKey(change.getConfigID(), change.getKey());
        if (!configuredFields.containsKey(configKey)) {
            log.warn("Noone listened to change in config: {}:{} was set to {}", change.getConfigID(), change.getKey(), change.getValue());
            return;
        }
        for (Field fieldsToUpdate: configuredFields.get(new FullKey(change.getConfigID(), change.getKey()))) {
            trySetFieldValue(fieldsToUpdate, change.getValue());
        }
        log.trace("Config changed: {}:{} set to {}", change.getConfigID(), change.getKey(), change.getValue());
    }
}
