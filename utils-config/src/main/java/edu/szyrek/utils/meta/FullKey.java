package edu.szyrek.utils.meta;

import java.util.Objects;

/**
 * Created by bebebaba on 2016-08-02.
 */
public class FullKey{
    public String configID;
    public String key;

    public FullKey(String configID, String key) {
        this.configID = configID;
        this.key = key;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj==null || !this.getClass().isAssignableFrom(obj.getClass())) {
            return false;
        } else {
            FullKey other = (FullKey) obj;
            return  Objects.equals(configID, other.configID) &&
                    Objects.equals(key, other.key);
        }
    }

    @Override
    public int hashCode(){
        return Objects.hash(configID, key);
    }

}