package edu.szyrek.utils.annotation;

import edu.szyrek.utils.config.adapter.JSONConfigType;
import edu.szyrek.utils.config.adapter.JSONConfigTypeAdapter;
import edu.szyrek.utils.config.adapter.StringAdapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by bebebaba on 2016-07-17.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigType {
    Class adapter() default StringAdapter.class;
}
