package edu.szyrek.utils.annotation;

import edu.szyrek.utils.config.adapter.StringAdapter;

import java.lang.annotation.*;

/**
 * Created by bebebaba on 2016-07-16.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Configurable {
    String key();
    String configID() default "MAIN_CONFIG";
    Class adapter() default StringAdapter.class;
}
