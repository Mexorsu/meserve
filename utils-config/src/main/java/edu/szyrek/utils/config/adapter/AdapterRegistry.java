package edu.szyrek.utils.config.adapter;

import edu.szyrek.utils.annotation.TypeAdapter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by bebebaba on 2016-07-17.
 */
@Slf4j
public class AdapterRegistry {
    private static Map<String, ITypeAdapter> adapters = new HashMap<>();

    public static void loadAdapters(Set<String> adapters) {
        for (String adapterClassName: adapters) {
            tryLoadAdapter(adapterClassName);
        }
    }

    public static boolean hasAdapterForName(String adapterClassName) {
        return adapters.containsKey(adapterClassName);
    }

    public static ITypeAdapter getAdapterForName(String adapterClassName) {
        return adapters.get(adapterClassName);
    }

    public static void tryLoadAdapter(String adapterClassName) {
        try {
            loadAdapter(adapterClassName);
        } catch (ClassNotFoundException e) {
            log.error("Failed to load adapter class {}", adapterClassName, e);
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("Failed to instantiate adapter {}", adapterClassName, e);
        }
    }

    public static void loadAdapter(String adapterClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        register(adapterClassName, (ITypeAdapter) Class.forName(adapterClassName).newInstance());
    }

    public static void register(String adapterClassName, ITypeAdapter adapterInstance) {
        try {
            adapters.put(adapterClassName, (ITypeAdapter) Class.forName(adapterClassName).newInstance());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            log.error("Failed to register {}", adapterClassName, e);
        }
    }

}
