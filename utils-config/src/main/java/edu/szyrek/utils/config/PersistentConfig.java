package edu.szyrek.utils.config;

import edu.szyrek.utils.File;

/**
 * Created by bebebaba on 2016-07-18.
 */
public interface PersistentConfig {
    public void persist(File file);
    public void loadFromFile(File file);
}
