package edu.szyrek.utils.config.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import edu.szyrek.utils.BoolParser;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import edu.szyrek.utils.exception.PropertyTypeMismatchException;
import edu.szyrek.utils.config.ReadableConfig;
import edu.szyrek.utils.config.StrongTypedConfig;

public class PropertyFileConfig extends StrongTypedConfig implements ReadableConfig {
    private final static String LIST_SEPARATOR = "[ \t]*,[ \t]*";
    private Properties properties = new Properties();
    private Properties defaults = new Properties();

   public PropertyFileConfig(Path propertiesFile, Path defaultsFile) {
        try (InputStream input = new FileInputStream(propertiesFile.toString())) {
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (defaultsFile != null) {
            try (InputStream input = new FileInputStream(defaultsFile.toString())) {
                defaults.load(input);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public Set<String> getKeys() {
        Set<String> result = new HashSet<>();
        properties.keySet().forEach(el -> {
            result.add((String)el);
        });
        return result;
    }

    public Object get(String key) throws NoSuchPropertyException {
        Object value = properties.getOrDefault(key, defaults.get(key));
        if (value == null) {
            throw new NoSuchPropertyException("Configuration property "+key+" is not defined and does not have a default value");
        } else {
            return value;
        }
    }

    @Override
    public boolean isSet(String key) {
        return properties.containsKey(key) && properties.get(key) != null;
    }

    public String getString(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        Object value = get(key);
        if (value instanceof String) {
            return (String)value;
        } else {
            throw new PropertyTypeMismatchException("Property "+key+" is of type "+value.getClass().getSimpleName()+" while String expected");
        }
    }
    @Override
    public Integer getInteger(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        Object value = get(key);
        if (value instanceof String) {
            try {
                return Integer.parseInt(((String)value));
            } catch (NumberFormatException e) {
                throw new PropertyTypeMismatchException(e.getMessage());
            }
        } else if (value instanceof Integer) {
            return (Integer) value;
        } else {
            throw new PropertyTypeMismatchException("Property "+key+" is of type "+value.getClass().getSimpleName()+" while Integer expected");
        }
    }
    @Override
    public Double getDouble(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        Object value = get(key);
        if (value instanceof String) {
            try {
                return Double.parseDouble(((String)value));
            } catch (NumberFormatException e) {
                throw new PropertyTypeMismatchException(e.getMessage());
            }
        } else if (value instanceof Double) {
            return (Double) value;
        } else {
            throw new PropertyTypeMismatchException("Property "+key+" is of type "+value.getClass().getSimpleName()+" while Double expected");
        }
    }
    @Override
    public Boolean getBoolean(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        Object value = get(key);
        if (value instanceof String) {
            try {
                return BoolParser.parseBool(((String)value));
            } catch (IllegalArgumentException e) {
                throw new PropertyTypeMismatchException(e.getMessage());
            }
        } else if (value instanceof Boolean) {
            return (Boolean) value;
        } else {
            throw new PropertyTypeMismatchException("Property "+key+" is of type "+value.getClass().getSimpleName()+" while Double expected");
        } 
    }
    @Override
    public List<String> getStrings(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        return Arrays.asList(getString(key).split(LIST_SEPARATOR));
    }
    @Override
    public List<Integer> getIntegers(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        try {
            return getStrings(key)
                .stream()
                .map(string->Integer.parseInt(string))
                .collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new PropertyTypeMismatchException(e.getMessage());
        }
    }
    @Override
    public List<Double> getDoubles(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        try {
            return getStrings(key)
                .stream()
                .map(string->Double.parseDouble(string))
                .collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new PropertyTypeMismatchException(e.getMessage());
        }
    }
    @Override
    public List<Boolean> getBooleans(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        try {
            return getStrings(key)
                .stream()
                .map(string->BoolParser.parseBool(string))
                .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            throw new PropertyTypeMismatchException(e.getMessage());
        }
    }
}
