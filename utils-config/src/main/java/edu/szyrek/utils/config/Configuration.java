package edu.szyrek.utils.config;

import edu.szyrek.utils.ConfigUpdateMessage;
import edu.szyrek.utils.config.impl.JSONConfig;
import edu.szyrek.utils.config.impl.JSONFileConfig;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import edu.szyrek.utils.meta.ConfigurableClassLoader;
import edu.szyrek.utils.meta.MetaConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bebebaba on 2016-07-27.
 */
@Slf4j
public class Configuration extends MetaConfiguration {
    public static final String DEFAULT_CONFIG_ID = "MAIN_CONFIG";
    public static final String META_CONFIG_ID = "META_CONFIG";
    public static final String DEFAULT_CONFIG_FILE_NAME = "main.conf";
    public static final String DEFAULT_CONFIG_FILE_LOCATION = System.getProperty("user.home");
    public static final String CONFIG_TYPES = "configTypes";
    public static final String TYPE_ADAPTERS = "typeAdapters";

    private static class Holder {
        public static Configuration instance = new Configuration();
    }

    public static Configuration MAIN() {
        return Holder.instance;
    }

    private Map<String, ReadWriteConfig> configRegistry = new ConcurrentHashMap<>();

    protected Configuration() {
        try {
            registerMetaConfiguration(tryLoadMetaConfiguration());
        } catch (IOException |ParseException|NoSuchPropertyException |URISyntaxException e) {
            log.error("Failed to log meta configuration", e);
        }

        try {
            loadMainConfiguration();
        } catch (FileNotFoundException |NoSuchPropertyException e) {
            log.error("Failed to load main configuration from "+DEFAULT_CONFIG_FILE_LOCATION+ edu.szyrek.utils.File.SEPARATOR+DEFAULT_CONFIG_FILE_NAME);
        }
    }

    private void registerMetaConfiguration(ReadableConfig metaConfig) throws URISyntaxException, IOException, NoSuchPropertyException, ParseException {
        register(META_CONFIG_ID, metaConfig);
    }

    private void loadMainConfiguration() throws FileNotFoundException, NoSuchPropertyException {
        edu.szyrek.utils.File mainConfig = edu.szyrek.utils.File.open(DEFAULT_CONFIG_FILE_LOCATION+ edu.szyrek.utils.File.SEPARATOR+DEFAULT_CONFIG_FILE_NAME);
        loadFromJSONFile(DEFAULT_CONFIG_ID, mainConfig);
    }

    public void loadFromJSON(String configID, JSONObject config) throws ParseException, NoSuchPropertyException {
        JSONConfig confiuration = new JSONConfig(config);
        register(configID, confiuration);
    }

    public void loadFromJSONFile(String configID, edu.szyrek.utils.File JSONFile) throws FileNotFoundException, NoSuchPropertyException {
        JSONFileConfig config = new JSONFileConfig(JSONFile);
        log.trace("Parsed "+JSONFile.getPath()+", found "+config.getKeys().size()+" values");
        register(configID, config);
    }

    public void register(String configID, ReadableConfig config) throws NoSuchPropertyException {
        log.trace("Registering {}", configID);
        if (configRegistry.containsKey(configID)) {
            configRegistry.put(configID,configRegistry.get(configID).merge(config));
        } else {
            configRegistry.put(configID, new JSONConfig(config));
        }
    }

    public <T> void set(String configID, String key, T value) {
        configRegistry.get(configID).set(key, value);
        ConfigurableClassLoader.notifyConfigChanged(new ConfigUpdateMessage<T>(configID, key, value));
    }

    public <T> T get(String configID, String key) throws NoSuchPropertyException {
        if (configID.length() > 0) {
            // get from specific config mapped by configID
            if (!configRegistry.containsKey(configID)) throw new NoSuchPropertyException(key);
            return configRegistry.get(configID).get(key);
        } else if (configRegistry.get(DEFAULT_CONFIG_ID).isSet(key)) {
            // look in main config first
            return configRegistry.get(DEFAULT_CONFIG_ID).get(key);
        } else {
            // look in all configs
            for(ReadableConfig config: configRegistry.values()) {
                if (config.isSet(key)) return config.get(key);
            }
            // if we reached here, it wasn't found
            throw new NoSuchPropertyException(key);
        }
    }

    public boolean isSet(String configID, String key) {
        if (configID.length() > 0 && configRegistry.containsKey(configID)) {
            // get from specific config mapped by configID
            return configRegistry.get(configID).isSet(key);
        } else if (configRegistry.get(DEFAULT_CONFIG_ID)!=null && configRegistry.get(DEFAULT_CONFIG_ID).isSet(key)) {
            // look in main config first
            return true;
        } else {
            // look in all configs
            for(ReadableConfig config: configRegistry.values()) {
                if (config.isSet(key)) return true;
            }
            // if we reached here, it wasn't found
            return false;
        }
    }


    private JSONConfig tryLoadMetaConfiguration() {
        try {
            return new JSONConfig(loadMetaConfiguration());
        } catch (IOException | ParseException e) {
            log.error("Failed to load metaconfiguration",e);
        }
        return new JSONConfig();
    }
}
