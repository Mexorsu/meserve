package edu.szyrek.utils.config.adapter;
import edu.szyrek.utils.annotation.TypeAdapter;
import edu.szyrek.utils.config.impl.JSONConfig;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by bebebaba on 2016-07-30.
 */
@Slf4j
@TypeAdapter(from = JSONObject.class, to = JSONConfigType.class)
public class JSONConfigTypeAdapter implements ITypeAdapter<JSONObject, JSONConfigType> {
    private JSONParser jsonParser = new JSONParser();

    @Override
    public JSONConfigType adapt(JSONObject valueFromFile, Class<? extends JSONConfigType> targetClass) throws AdaptationException {
        try {
            JSONConfigType result = targetClass.newInstance();
            result.fromJSON(valueFromFile);
            return result;
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("Error: ",e);
            throw new AdaptationException(valueFromFile.toJSONString(), targetClass);
        }
    }
}
