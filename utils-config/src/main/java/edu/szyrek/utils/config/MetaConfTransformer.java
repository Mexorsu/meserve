package edu.szyrek.utils.config;


import edu.szyrek.utils.meta.MetaAnnotationProcessor;
import edu.szyrek.utils.meta.MetaConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.plugins.shade.resource.ResourceTransformer;
import org.codehaus.plexus.util.IOUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

/**
 * A resource processor that appends content for a resource, separated by a newline.
 *
 */

/**
 * Created by bebebaba on 2016-08-20.
 */

@Slf4j
public class MetaConfTransformer implements ResourceTransformer {
        String resource;
        protected static MetaConfiguration metaConfig;

        public boolean canTransformResource( String r )
        {
            if ( resource != null && resource.equalsIgnoreCase( r ) )
            {
                return true;
            }

            return false;
        }

        public void processResource( String resource, InputStream is, List relocators )
                throws IOException
        {
            if (metaConfig==null) {
                metaConfig = MetaConfiguration.fromStrean(is);
            } else {
                MetaConfiguration nextMetaConfig = MetaConfiguration.fromStrean(is);
                metaConfig = metaConfig.merge(nextMetaConfig);
            }
            is.close();
        }

        public boolean hasTransformedResource()
        {
            return metaConfig!=null && metaConfig.size() > 0;
        }

        public void modifyOutputStream( JarOutputStream jos )
                throws IOException
        {
            log.info("Adding {} to {}", metaConfig.toJSONString(), resource);
            jos.putNextEntry( new JarEntry( resource ) );
            if (metaConfig!=null) {
                IOUtil.copy(new ByteArrayInputStream(metaConfig.toJSONString().getBytes()), jos);
            }
            metaConfig = null;
        }
}
