package edu.szyrek.utils.config.adapter;

import edu.szyrek.utils.annotation.ConfigType;
import org.json.simple.JSONObject;

/**
 * Created by bebebaba on 2016-07-30.
 */
@ConfigType(adapter = JSONConfigTypeAdapter.class)
public interface JSONConfigType {
    public void fromJSON(JSONObject json);
}
