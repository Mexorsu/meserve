package edu.szyrek.utils.config;

/**
 * Created by bebebaba on 2016-07-30.
 */
public interface ReadWriteConfig extends WritableConfig, ReadableConfig {
}
