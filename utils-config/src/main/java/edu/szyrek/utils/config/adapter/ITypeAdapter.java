package edu.szyrek.utils.config.adapter;

/**
 * Created by bebebaba on 2016-07-16.
 */
public interface ITypeAdapter<FROM,TO> {
    public class AdaptationException extends Exception {
        public AdaptationException(String value, Class clazz) {
            super("Failed to adapt "+value+" to "+clazz.getCanonicalName());
        }
    }
    TO adapt(FROM valueFromFile, Class<? extends TO> targetClass) throws AdaptationException;
}
