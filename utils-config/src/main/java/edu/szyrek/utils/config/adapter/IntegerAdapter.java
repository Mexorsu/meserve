package edu.szyrek.utils.config.adapter;

import edu.szyrek.utils.annotation.TypeAdapter;

/**
 * Created by bebebaba on 2016-07-17.
 */
@TypeAdapter(from = String.class, to = Integer.class)
public class IntegerAdapter implements ITypeAdapter<String, Integer> {
    public Integer adapt(String valueFromFile) {
        return Integer.parseInt(valueFromFile);
    }

    @Override
    public Integer adapt(String valueFromFile, Class<? extends Integer> targetClass) throws AdaptationException {
        return adapt(valueFromFile);
    }
}
