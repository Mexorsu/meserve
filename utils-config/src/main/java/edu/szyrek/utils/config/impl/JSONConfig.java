package edu.szyrek.utils.config.impl;

import edu.szyrek.utils.config.ReadWriteConfig;
import edu.szyrek.utils.config.ReadableConfig;
import edu.szyrek.utils.config.StrongTypedConfig;
import edu.szyrek.utils.config.WritableConfig;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by bebebaba on 2016-07-16.
 */
@Slf4j
public class JSONConfig extends StrongTypedConfig implements ReadWriteConfig {
    protected JSONObject configuration = new JSONObject();
    private final static String PATH_SEPARATOR = ".";

    public JSONConfig() {

    }

    public JSONConfig(ReadableConfig config) {
        try {
            for (String key: config.getKeys()) {
                this.configuration.put(key, config.get(key));
            }
        } catch (NoSuchPropertyException e) {
            log.error("Failed to cinstruct JSONConfig from {}",config.toString());
        }
    }

    public JSONConfig(String json) throws ParseException{
        JSONParser parser = new JSONParser();
        this.configuration = (JSONObject) parser.parse(json);
    }

    public JSONConfig(JSONObject json) throws ParseException{
        JSONParser parser = new JSONParser();
        this.configuration = json;
    }

    public void loadJSONInputStream(FileInputStream input) throws IOException, ParseException, NoSuchPropertyException {
        StringBuffer buffer = new StringBuffer();
        DataInputStream stream = new DataInputStream(new BufferedInputStream(input));
        while(stream.available()>0) {
            buffer.append(stream.readLine());
        }
        loadJSON(buffer.toString());
    }

    public void loadJSON(String json) throws ParseException, NoSuchPropertyException {
        JSONConfig newConfiguration = new JSONConfig(json);
        for (String key: newConfiguration.getKeys()) {
            configuration.put(key, newConfiguration.get(key));
        }
    }

    public ReadWriteConfig merge(ReadableConfig other) throws NoSuchPropertyException {
        log.trace("before merge: "+this.getKeys().size());
        List<String> allKeys = new ArrayList<>();
        allKeys.addAll(getKeys());
        allKeys.addAll(other.getKeys());

        JSONConfig config = new JSONConfig();

        for(String key: allKeys) {
            if (other.isSet(key)) {
                log.trace("Merging key "+key);
                config.set(key, other.get(key));
            } else {
                log.trace("new key: "+key);
                config.set(key, other.get(key));
            }
        }
        log.trace("after merge: "+this.getKeys().size());
        return config;
    }

    private <T> void mergeKey(ReadableConfig otherConfig, String key) throws NoSuchPropertyException {
        T otherValue = otherConfig.get(key);
        T thisValue = this.get(key);
        try {
            if (thisValue instanceof Collection && otherValue instanceof Collection) {
                Collection<T> ourCollection = (Collection<T>) thisValue;
                Collection<T> otherCollection = (Collection<T>) thisValue;
                ourCollection.addAll(otherCollection);
            } else if (thisValue instanceof Collection) {
                Collection<T> ourCollection = (Collection<T>) thisValue;
                ourCollection.add(otherValue);
            } else if (otherValue instanceof Collection) {
                Collection<T> otherCollection = (Collection<T>) otherValue;
                otherCollection.add(thisValue);
            } else {
                System.out.println("Overriding config for $key from ${thisValue} to "+otherValue);
                configuration.put(key, otherValue);
            }
        }catch (Exception e) {
           log.error("Failed to merge configuration for $key, values are: {} and {}", thisValue, otherValue);
        }
    }

    @Override
    public <T> void set(String key, T value) {
        configuration.put(key, value);
    }
    @Override
    public boolean isSet(String key) {
        return configuration.containsKey(key);
    }
    @Override
    public <T> T get(String keyPath) {
        return get(keyPath, null);
    }
    @Override
    public <T> T get(String keyPath, T defaultValue) {
        String path = keyPath;
        JSONObject current = configuration;

        T result = null;

        while(!path.isEmpty()) {
            String nextComponent = path;
            if (path.contains(PATH_SEPARATOR)) {
                nextComponent = path.substring(0, path.indexOf(PATH_SEPARATOR));
                path = path.substring(path.indexOf(PATH_SEPARATOR)+1);

                if (nextComponent.matches(".*\\[[0-9]*\\]")) {
                    JSONArray array = (JSONArray) current.get(nextComponent.substring(0,nextComponent.indexOf('[')));
                    current = (JSONObject) array.get(Integer.parseInt(nextComponent.substring(nextComponent.indexOf('[')+1,nextComponent.indexOf(']'))));
                } else {
                    current = (JSONObject) current.get(nextComponent);
                }
            } else {
                if (nextComponent.matches(".*\\[[0-9]*\\]")) {
                    JSONArray array = (JSONArray) current.get(nextComponent.substring(0,nextComponent.indexOf('[')));
                    result = (T) array.get(Integer.parseInt(nextComponent.substring(nextComponent.indexOf('[')+1,nextComponent.indexOf(']'))));
                    break;
                } else {
                    result = (T) current.get(nextComponent);
                    break;
                }
            }
        }

        return result != null ? result : defaultValue;
    }
    @Override
    public Set<String> getKeys() {
        return configuration.keySet();
    }

}
