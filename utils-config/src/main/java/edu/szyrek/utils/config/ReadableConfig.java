package edu.szyrek.utils.config;

import java.util.Set;

import edu.szyrek.utils.exception.NoSuchPropertyException;

public interface ReadableConfig {
    public <T> T get(String key) throws NoSuchPropertyException;
    public boolean isSet(String key);
    public Set<String> getKeys();
}
