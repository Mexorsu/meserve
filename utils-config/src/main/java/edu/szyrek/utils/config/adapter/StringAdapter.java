package edu.szyrek.utils.config.adapter;

import edu.szyrek.utils.annotation.TypeAdapter;

/**
 * Created by bebebaba on 2016-07-16.
 */
@TypeAdapter(from = String.class, to = Integer.class)
public class StringAdapter <FROM> implements ITypeAdapter<FROM,String> {
    public String adapt(FROM valueFromFile) {
        return valueFromFile.toString();
    }

    @Override
    public String adapt(FROM valueFromFile, Class<? extends String> targetClass) throws AdaptationException {
        return adapt(valueFromFile);
    }
}
