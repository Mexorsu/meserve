package edu.szyrek.utils.config.impl;


import edu.szyrek.utils.File;
import edu.szyrek.utils.config.PersistentConfig;
import edu.szyrek.utils.config.ReadableConfig;
import edu.szyrek.utils.config.WritableConfig;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.file.Path;

/**
 * Created by bebebaba on 2016-07-18.
 */
@Slf4j
public class JSONFileConfig extends JSONConfig implements ReadableConfig, WritableConfig, PersistentConfig {
    private File jsonFile;

    public JSONFileConfig(File configFile) throws FileNotFoundException {
        this.jsonFile = File.open(configFile.toString());
        loadJSONFile(configFile);
    }

    public void loadJSONFile(File file) {
        try (FileInputStream in = file.getInputStream()) {
            loadJSONInputStream(in);
        } catch (final ParseException e){
            log.error("Failed to parse JSON Configuration from {}", file, e);
        } catch (NoSuchPropertyException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void persist(File file) {
        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file.getWrapped())))) {
            out.writeUTF(configuration.toJSONString());
        } catch (FileNotFoundException e) {
            log.error("Couldn't persist config file {}", file.getDescription(), e);
        } catch (IOException e) {
            log.error("Couldn't persist config file {}", file.getDescription(), e);
        }
    }

    @Override
    public void loadFromFile(File file) {
        loadJSONFile(file);
    }
}
