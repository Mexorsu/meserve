package edu.szyrek.utils.config.adapter;

import java.lang.reflect.Type;

/**
 * Created by bebebaba on 2016-07-16.
 */
public class TypeAdapterException extends Error {
    public TypeAdapterException(String msg) {
        super(msg);
    }
}
