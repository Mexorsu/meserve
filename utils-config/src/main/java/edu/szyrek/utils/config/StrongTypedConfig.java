package edu.szyrek.utils.config;

import edu.szyrek.utils.BoolParser;
import edu.szyrek.utils.config.adapter.IntegerAdapter;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import edu.szyrek.utils.exception.PropertyTypeMismatchException;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by bebebaba on 2016-07-16.
 */
public abstract class StrongTypedConfig extends ConfigWithDefaults implements ReadableConfig {
    @Override
    public abstract  <T> T get(String key) throws NoSuchPropertyException;
    @Override
    public abstract boolean isSet(String key);
    @Override
    public abstract  Set<String> getKeys();

    public String getString(String key) throws NoSuchPropertyException, PropertyTypeMismatchException {
        return (String)get(key);
    }

    public String getString(String key, String defaultValue) throws NoSuchPropertyException, PropertyTypeMismatchException {
        return (String)get(key, defaultValue);
    }

    public Integer getInteger(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Integer)get(key);
    }

    public Integer getInteger(String key, Integer defaultValue) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Integer)get(key, defaultValue);
    }

    public Double getDouble(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Double) get(key);
    }

    public Double getDouble(String key, Double defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Double) get(key, defVal);
    }

    public Boolean getBoolean(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Boolean) get(key);
    }

    public Boolean getBoolean(String key, Boolean defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Boolean) get(key, defVal);
    }

    public Collection<String> getStrings(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<String>)get(key);
    }

    public Collection<String> getStrings(String key, Collection<String> defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<String>)get(key, defVal);
    }

    public Collection<Integer> getIntegers(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Integer>)get(key);
    }

    public Collection<Integer> getIntegers(String key, Collection<Integer> defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Integer>)get(key, defVal);
    }

    public Collection<Double> getDoubles(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Double>)get(key);
    }

    public Collection<Double> getDoubles(String key, Collection<Double> defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Double>)get(key, defVal);
    }

    public Collection<Boolean> getBooleans(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Boolean>)get(key);
    }

    public Collection<Boolean> getBooleans(String key, Collection<Boolean> defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (Collection<Boolean>)get(key, defVal);
    }

    public <T> List<T> getList(String key) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (List<T>)get(key);
    }

    public <T> List<T> getList(String key, List<T> defVal) throws NoSuchPropertyException, PropertyTypeMismatchException{
        return (List<T>)get(key, defVal);
    }
}
