package edu.szyrek.utils.config.adapter;

/**
 * Created by bebebaba on 2016-07-17.
 */
public class MissingAdapterException extends Exception {
    public MissingAdapterException(Class<?> from, Class<?> to) {
        super("Missing adapter from "+from.getSimpleName()+" to "+to.getSimpleName());
    }
    public MissingAdapterException(String name) {
        super("Missing adapter named "+name);
    }
}
