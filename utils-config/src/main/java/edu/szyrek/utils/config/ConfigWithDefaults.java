package edu.szyrek.utils.config;

import edu.szyrek.utils.exception.NoSuchPropertyException;

import java.util.Set;

/**
 * Created by bebebaba on 2016-07-16.
 */
public abstract class ConfigWithDefaults implements ReadableConfig {
    @Override
    abstract public <T> T get(String key) throws NoSuchPropertyException;
    @Override
    public abstract boolean isSet(String key);
    @Override
    public abstract  Set<String> getKeys();

    public <T> T get(String key, T defaultValue) throws NoSuchPropertyException {
        if (isSet(key)) {
            return get(key);
        } else {
            return defaultValue;
        }
    }
}
