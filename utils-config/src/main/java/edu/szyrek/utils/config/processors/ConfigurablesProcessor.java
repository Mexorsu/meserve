package edu.szyrek.utils.config.processors;

import java.io.*;

import edu.szyrek.utils.config.Configuration;
import edu.szyrek.utils.annotation.ConfigType;
import edu.szyrek.utils.annotation.TypeAdapter;
import edu.szyrek.utils.meta.MetaAnnotationProcessor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Set;

/**
 * Created by bebebaba on 2016-07-16.
 */

@Slf4j
@SupportedAnnotationTypes({"edu.szyrek.utils.annotation.ConfigType", "edu.szyrek.utils.annotation.TypeAdapter", "edu.utils.bootstrap.annotation.App"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class ConfigurablesProcessor extends MetaAnnotationProcessor {
    private static JSONArray types = new JSONArray();
    private static JSONArray adapters = new JSONArray();

    static {
        types = metaConfig.containsKey(Configuration.CONFIG_TYPES) ? (JSONArray) metaConfig.get(Configuration.CONFIG_TYPES) : types;
        adapters = metaConfig.containsKey(Configuration.TYPE_ADAPTERS) ? (JSONArray) metaConfig.get(Configuration.TYPE_ADAPTERS) : adapters;
    }

    @Override
    public boolean tryProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        TypeElement processedAnnotation = null;
        try {
            for (TypeElement annotation : annotations) {
                processAnnotation(annotation, roundEnv);
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            if (processedAnnotation == null) {
                log.error("Failed to initialize {}", ConfigurablesProcessor.class.getSimpleName(), e);
            }
            log.error("Failed to process {} ", processedAnnotation.getSimpleName(), e);
            return false;
        }

        metaConfig.put(Configuration.CONFIG_TYPES,types);
        metaConfig.put(Configuration.TYPE_ADAPTERS,adapters);
        return true;
    }

    private void processAnnotation(TypeElement annotation, RoundEnvironment roundEnv) throws NoSuchFieldException, IllegalAccessException {
        for (Element el : roundEnv.getElementsAnnotatedWith(annotation)) {
            if (TypeAdapter.class.getSimpleName().equals(annotation.getSimpleName().toString())) {
                addAdapter(getFullName(el));
            } else if (ConfigType.class.getSimpleName().equals(annotation.getSimpleName().toString())) {
                addType(getFullName(el));
            }
        }
    }


    private static void addType(String className) {
        JSONObject type = new JSONObject();
        type.put("class", "\"" + className + "\"");

        synchronized (types) {
            if (!types.contains(type)) {
                types.add(type);
            }
        }
    }

    private static void addAdapter(String className) {
        JSONObject adapter = new JSONObject();
        adapter.put("class", "\"" + className + "\"");

        synchronized (adapters) {
            if (!adapters.contains(adapter)) {
                adapters.add(adapter);
            }
        }
    }

}
