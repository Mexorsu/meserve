package edu.szyrek.utils.config;

import edu.szyrek.utils.exception.NoSuchPropertyException;

/**
 * Created by bebebaba on 2016-07-16.
 */
public interface WritableConfig extends ReadableConfig {
    public <T> void set(String key, T value);
    public ReadWriteConfig merge(ReadableConfig other) throws NoSuchPropertyException;
}
