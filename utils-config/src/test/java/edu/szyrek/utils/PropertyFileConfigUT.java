package edu.szyrek.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

import edu.szyrek.utils.config.impl.PropertyFileConfig;
import edu.szyrek.utils.exception.NoSuchPropertyException;
import edu.szyrek.utils.exception.PropertyTypeMismatchException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class PropertyFileConfigUT {
    private final static String STRING_PROPERTY_KEY = "string";
    private final static String INTEGER_PROPERTY_KEY = "integer";
    private final static String DOUBLE_PROPERTY_KEY = "double";
    private final static String BOOL_PROPERTY_KEY = "boolean";
    private final static String LIST_PROPERTY_KEY = "list";
    private final static String BOOL_LIST_PROPERTY_KEY = "bool_list";
    private final static String DEFAULT_PROPERTY_KEY = "default_value";
    private final static String OVERRIDEN_PROPERTY_KEY = "overriden_value";
    private final static String UNEXISTING_PROPERTY_KEY = "bebe";

    private final static String STRING_PROPERTY_VALUE = "Hello world";
    private final static Integer INTEGER_PROPERTY_VALUE = 123;
    private final static Double DOUBLE_PROPERTY_VALUE = 12.345;
    private final static Boolean BOOL_PROPERTY_VALUE = true;
    private final static Integer[] LIST_PROPERTY_VALUE = new Integer[]{1,2,3,4,5,6};
    private final static Boolean[] BOOL_LIST_PROPERTY_VALUE = new Boolean[]{true, false, true, false};
    private final static String DEFAULT_PROPERTY_VALUE = "default";
    private final static String OVERRIDEN_PROPERTY_VALUE = "overriden";

    private final static String TEST_PROPS_FILE_NAME = "test.properties";
    private final static String TEST_DEFS_FILE_NAME = "test.defaults";

    private static PropertyFileConfig testConfig;
    private static Path propertiesFile;
    private static Path defaultsFile;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setup() throws Exception {
        ClassLoader cl = PropertyFileConfigUT.class.getClassLoader();
        String propsPath;
        String defsPath;
        if(System.getProperty("os.name").toLowerCase().contains("win")) {
            propsPath = cl.getResource(TEST_PROPS_FILE_NAME).toString().substring(6);
            defsPath = cl.getResource(TEST_DEFS_FILE_NAME).toString().substring(6);
        } else {
            propsPath = cl.getResource(TEST_PROPS_FILE_NAME).toString().substring(5);
            defsPath = cl.getResource(TEST_DEFS_FILE_NAME).toString().substring(5);
        }
        propertiesFile = Paths.get(propsPath);
        defaultsFile = Paths.get(defsPath);
        System.out.println("props: "+propsPath);
        System.out.println("defs: "+defsPath);
    }   
    @Test
    public void testTestSetup() {
        testConfig = new PropertyFileConfig(propertiesFile, defaultsFile);
        Assert.assertNotNull(testConfig);
    }
    @Test
    public void testBasicProperties() throws NoSuchPropertyException, PropertyTypeMismatchException {
	    Assert.assertEquals(testConfig.getString(STRING_PROPERTY_KEY), STRING_PROPERTY_VALUE);
	    Assert.assertEquals(testConfig.getInteger(INTEGER_PROPERTY_KEY), INTEGER_PROPERTY_VALUE);
	    Assert.assertEquals(testConfig.getDouble(DOUBLE_PROPERTY_KEY), DOUBLE_PROPERTY_VALUE);
	    Assert.assertEquals(testConfig.getBoolean(BOOL_PROPERTY_KEY), BOOL_PROPERTY_VALUE);
        Assert.assertArrayEquals(testConfig.getIntegers(LIST_PROPERTY_KEY).toArray(), LIST_PROPERTY_VALUE);
	    Assert.assertArrayEquals(testConfig.getBooleans(BOOL_LIST_PROPERTY_KEY).toArray(), BOOL_LIST_PROPERTY_VALUE);
    }
    @Test
    public void testDefauts() throws NoSuchPropertyException, PropertyTypeMismatchException {
        Assert.assertEquals(testConfig.getString(DEFAULT_PROPERTY_KEY), DEFAULT_PROPERTY_VALUE);
        Assert.assertEquals(testConfig.getString(OVERRIDEN_PROPERTY_KEY), OVERRIDEN_PROPERTY_VALUE);
    }
    @Test
    public void testNotExisting() throws NoSuchPropertyException {
        expected.expect(NoSuchPropertyException.class);
        testConfig.get(UNEXISTING_PROPERTY_KEY);
    }
    @Test
    public void testWrongType() throws NoSuchPropertyException, PropertyTypeMismatchException {
        expected.expect(PropertyTypeMismatchException.class);
        testConfig.getInteger(STRING_PROPERTY_KEY);
    }
}
