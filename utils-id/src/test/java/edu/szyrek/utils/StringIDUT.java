package edu.szyrek.utils;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class StringIDUT {
    private static final String testString =  "BABA";
    private static final String differentString =  "BABA1";

    private static ID testId;
    private static ID differentId;
    private static ID sameId;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setupIDs() throws IDTakenException {
        testId = StringID.create(testString);
        differentId = StringID.create(differentString);
        sameId = StringID.get(testString);
    }

    @Test
    public void testStringIDCollistion() throws IDTakenException {
        expected.expect(IDTakenException.class);
        ID colliding = StringID.create(testString);
    }

    @Test
    public void testStringID() throws IDTakenException {
        Assert.assertEquals(testId, sameId);
        Assert.assertNotEquals(testId, differentId);
        Assert.assertEquals(testId.hashCode(), sameId.hashCode());
    }
    @Test
    public void testRelease() throws IDTakenException {
        ID.release(testId);
        ID notColligingAnymore = StringID.create(testString);
    }

}
