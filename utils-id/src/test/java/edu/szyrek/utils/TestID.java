package edu.szyrek.utils;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class TestID extends ID<String> {
    public static final String testString =  "TEST";

    public TestID() throws IDTakenException {
        super();
    }


    public TestID(String val) throws IDTakenException {
        super(val);
    }
}