package edu.szyrek.utils;

import org.junit.*;
import org.junit.rules.ExpectedException;

/**
 * Created by bebebaba on 2016-08-12.
 */
public class GenericIDUT {
    private static final Integer testInt =  6;
    private static final Integer differentInt =  7;
    private static final String testString =  "BABA";
    private static final String differentString =  "BABA1";

    private static ID<Integer> intId;
    private static ID<Integer> differentIntId;
    private static ID<Integer> sameIntId;

    private static ID<String> stringId;
    private static ID<String> differentStringId;
    private static ID<String> sameStringId;
    private static TestID customID;

    @BeforeClass
    public static void setupIDs() throws IDTakenException {
        intId = ID.create(testInt);
        differentIntId = ID.create(differentInt);
        sameIntId = ID.get(ID.class, testInt);
        stringId = ID.create(testString);
        differentStringId = ID.create(differentString);
        sameStringId = ID.get(ID.class, testString);
        customID = new TestID(TestID.testString);
    }

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Test
    public void testRelease() throws IDTakenException {
        ID.release(ID.get(ID.class, testInt));
        ID.release(ID.get(ID.class, differentInt));

        ID.release(ID.get(ID.class, testString));
        ID.release(ID.get(ID.class, differentString));

        ID.release(ID.get(TestID.class, TestID.testString));
        setupIDs();
    }

    @Test
    public void testGet() throws IDTakenException {
        ID<Integer> fetchedIntID = ID.get(ID.class, testInt);
        Assert.assertEquals(fetchedIntID, intId);
        ID<Integer> fetchedStringID = ID.get(ID.class, testString);
        Assert.assertEquals(fetchedStringID, stringId);
        Assert.assertEquals(ID.get(TestID.class, TestID.testString), customID);
    }

    @Test
    public void testCustomCollision() throws IDTakenException {
        expected.expect(IDTakenException.class);
        TestID collidingID = new TestID(TestID.testString);
    }

    @Test
    public void testDifferentClassIDWithSameImpl() throws IDTakenException {
        class MyID extends ID<String> {
            public MyID() throws IDTakenException {
                super(testString);
            }
        }
        class MyID2 extends ID<String> {
            public MyID2() throws IDTakenException {
                super(testString);
            }
        }
        ID<String> first = new MyID();
        ID<String> second = new MyID2();
        Assert.assertNotEquals(first, second);
    }

    @Test
    public void testIntegerIDCollistion() throws IDTakenException {
        expected.expect(IDTakenException.class);
        ID<Integer> collidingID = ID.create(testInt);
    }

    @Test
    public void testStringIDCollistion() throws IDTakenException {
        expected.expect(IDTakenException.class);
        ID<String> collidingID = ID.create(testString);
    }

    @Test
    public void testIntegerID() throws IDTakenException {
        Assert.assertEquals(intId,  sameIntId);
        Assert.assertNotEquals(intId,  differentIntId);
        Assert.assertEquals(intId.hashCode(), sameIntId.hashCode());
    }
    @Test
    public void testStringID() throws IDTakenException {
        Assert.assertEquals(stringId,  sameStringId);
        Assert.assertNotEquals(stringId,  differentStringId);
        Assert.assertEquals(stringId.hashCode(), sameStringId.hashCode());
    }
}
