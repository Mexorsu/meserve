package edu.szyrek.utils;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class IntegerIDUT {
    private static final Integer testInt =  6;
    private static final Integer differentInt =  7;
    private static final int RANDOM_FELLAS_COUNT = 100;

    private static ID testId;
    private static ID differentId;
    private static ID sameId;
    private static IntegerID[] fellas;


    @Rule
    public ExpectedException expected = ExpectedException.none();

    @BeforeClass
    public static void setupIDs() throws IDTakenException {
        testId = IntegerID.create(testInt);
        differentId = IntegerID.create(differentInt);
        sameId = IntegerID.get(testInt);
        fellas = new IntegerID[RANDOM_FELLAS_COUNT];
        for (int i = 0; i < RANDOM_FELLAS_COUNT; i++){
            fellas[i] = (IntegerID) IntegerID.create();
        }
    }

    @Test
    public void testStringIDCollistion() throws IDTakenException {
        expected.expect(IDTakenException.class);
        ID colliding = IntegerID.create(testInt);
    }

    @Test
    public void testStringID() throws IDTakenException {
        Assert.assertEquals(testId, sameId);
        Assert.assertNotEquals(testId, differentId);
        Assert.assertEquals(testId.hashCode(), sameId.hashCode());
    }
    @Test
    public void testRelease() throws IDTakenException {
        ID.release(testId);
        for (int i = 0; i < RANDOM_FELLAS_COUNT; i++){
            ID.release(fellas[i]);
        }
        for (int i = 0; i < RANDOM_FELLAS_COUNT; i++){
            ID.create(fellas[i].getImpl());
        }
        ID notColligingAnymore = IntegerID.create(testInt);
    }
}
