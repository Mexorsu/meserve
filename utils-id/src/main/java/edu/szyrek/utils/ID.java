package edu.szyrek.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by bebebaba on 2016-08-12.
 */
public class ID<T> {
    protected T _idImpl = null;
    private static final HashMap<Class<? extends ID>, HashSet<Object>> _assignedValues = new HashMap<>();

    private static Lock idGenerationLock = new ReentrantLock();

    protected ID() {
    }

    public static ID get(Class<? extends ID> clazz, Object value) {
        ID res = null;
        if (clazz.equals(ID.class)) {
            res = new ID();
        } else {
            try {
                res = clazz.getDeclaredConstructor().newInstance();
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
        if (res==null){
            throw new RuntimeException("Could not instanciate "+clazz);
        }
        res._idImpl = value;
        return res;
    }

    public static <E> ID<E> create(E value) throws IDTakenException{
        return new ID<E>(value);
    }
    protected ID(T val) throws IDTakenException {
        initWith(val);
    }



    public static void release(ID id) {
        if (_assignedValues.get(id.getClass())!=null) {
            _assignedValues.get(id.getClass()).remove(id._idImpl);
            if (_assignedValues.get(id.getClass()).isEmpty()) {
                _assignedValues.remove(id.getClass());
            }
        }
    }

    protected void initWith(T val) throws IDTakenException {
        idGenerationLock.lock();
        if (_assignedValues.get(this.getClass())==null) {
            _assignedValues.put(this.getClass(), new HashSet<>());
        }
        try {
            this._idImpl = val;
            if (_assignedValues.get(this.getClass()).contains(val)) {
                this._idImpl = null;
                throw new IDTakenException();
            }
            _assignedValues.get(this.getClass()).add(val);

        } catch (IDTakenException e) {
            throw e;
        } finally {
            idGenerationLock.unlock();
        }
    }

    public void lock() {
        idGenerationLock.lock();
    }

    public void unlock() {
        idGenerationLock.unlock();
    }

    @Override
    public boolean equals(Object obj){
        if (obj==null || !this.getClass()
                .isAssignableFrom(obj.getClass())){
            return false;
        }
        ID<T> other = (ID<T>)obj;
        return this._idImpl.equals(other._idImpl);
    }

    @Override
    public int hashCode(){
        return Objects.hash(_idImpl);
    }

    @Override
    public void finalize(){
        _assignedValues.remove(this);
    }

}
