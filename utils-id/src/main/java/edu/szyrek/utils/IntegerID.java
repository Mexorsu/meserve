package edu.szyrek.utils;

import edu.szyrek.utils.random.Random;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;

/**
 * Created by bebebaba on 2016-08-13.
 */
@Slf4j
public class IntegerID extends ID<Integer> {
    protected IntegerID() {}
    protected IntegerID(Integer val) throws IDTakenException {
        super(val);
    }

    public static ID create(Integer value) throws IDTakenException {
        return new IntegerID(value);
    }

    public static ID get(Integer value) throws IDTakenException {
        return (IntegerID) ID.get(IntegerID.class, value);
    }

    protected void initWithRandomInteger() {
        while (this._idImpl==null) {
            try {
                initWith(Random.get(Integer.class));
            } catch (IDTakenException e) {
                e.printStackTrace();
            }

        }
    }

    public static ID create() {
        IntegerID result = new IntegerID();
        result.initWithRandomInteger();
        return result;
    }

    int getImpl() {
        return _idImpl;
    }
}
