package edu.szyrek.utils;

import edu.szyrek.utils.random.Random;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bebebaba on 2016-08-12.
 */
@Slf4j
public class StringID extends ID<String> {
    protected StringID() {}

    private StringID(String value) throws IDTakenException {
        super(value);
    }

    public static ID create(String value) throws IDTakenException {
        return new StringID(value);
    }

    public static ID get(String value) throws IDTakenException {
        return (StringID) ID.get(StringID.class, value);
    }

    protected void initWithRandomString() {
        while (this._idImpl==null) {
            try {
                initWith(Random.get(String.class));
            } catch (IDTakenException e) {
                e.printStackTrace();
            }

        }
    }

    public static StringID getRandom() {
        StringID result = new StringID();
        result.initWithRandomString();
        return result;
    }
}
