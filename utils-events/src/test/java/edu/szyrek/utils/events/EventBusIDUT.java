package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.IntegerID;
import edu.szyrek.utils.random.Random;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class EventBusIDUT {
    private static int testInt;
    private static int different;

    private static ID testEventBusID;
    private static ID sameID;
    private static ID differentID;

    @Rule
    public ExpectedException expectation = ExpectedException.none();

    @BeforeClass
    public static void setupEventBusIDs() throws IDTakenException {
        testInt = Random.get(Integer.class);
        do {
            different = Random.get(Integer.class);
        } while (different==testInt);
        testEventBusID = EventBusID.create(testInt);
        differentID = EventBusID.create(different);
        sameID = ID.get(EventBusID.class, testInt);
    }

    @Test
    public void testClassUniqueness() throws IDTakenException {
        IntegerID differentClass = (IntegerID) IntegerID.create(testInt);
        ID.release(differentClass);
        expectation.expect(IDTakenException.class);
        testEventBusID = EventBusID.create(testInt);
    }

    @Test
    public void testUniqueness() throws IDTakenException {
        expectation.expect(IDTakenException.class);
        testEventBusID = EventBusID.create(testInt);
    }


    @Test
    public void testEventBusID(){
        Assert.assertEquals(testEventBusID, sameID);
        Assert.assertNotEquals(testEventBusID, differentID);
        Assert.assertEquals(testEventBusID.hashCode(), sameID.hashCode());
    }

}
