package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.IntegerID;
import edu.szyrek.utils.random.IntegerBounds;
import edu.szyrek.utils.random.Random;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class EventIDUT {
    private static int testInt;
    private static int different;

    private static ID testEventID;
    private static ID sameID;
    private static ID differentID;

    @Rule
    public ExpectedException expectation = ExpectedException.none();

    @BeforeClass
    public static void setupEventIDs() throws IDTakenException {
        testInt = Random.get(Integer.class);
        do {
            different = Random.get(Integer.class);
        } while (different==testInt);
        testEventID = EventID.create(testInt);
        differentID = EventID.create(different);
        sameID = ID.get(EventID.class, testInt);
    }

    @Test
    public void testClassUniqueness() throws IDTakenException {
        IntegerID differentClass = (IntegerID) IntegerID.create(testInt);
        ID.release(differentClass);
        expectation.expect(IDTakenException.class);
        testEventID = EventID.create(testInt);
    }

    @Test
    public void testUniqueness() throws IDTakenException {
        expectation.expect(IDTakenException.class);
        testEventID = EventID.create(testInt);
    }


    @Test
    public void testEventID(){
        Assert.assertEquals(testEventID, sameID);
        Assert.assertNotEquals(testEventID, differentID);
        Assert.assertEquals(testEventID.hashCode(), sameID.hashCode());
    }

}
