package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Created by bebebaba on 2016-08-14.
 */
@Slf4j
public class EventBusUT  {
    public static final String TEST_EVENT_BUS_ID_NAME = "DUPA";

    private ID testEventID;
    private ID randomEventID;

    private static CompletableFuture<Boolean> promise = new CompletableFuture<>();
    private static CompletableFuture<Boolean> childPromise = new CompletableFuture<>();

    public EventBusUT() throws IDTakenException {
        testEventID = EventBusID.create(TEST_EVENT_BUS_ID_NAME);
        randomEventID = EventBusID.create();
    }

    private static class ExampleListener  implements EventListener<ExampleEvent> {
        @Override
        public void handle(ExampleEvent event) {
            log.info("ExampleListener got event {}", event.toString());
            EventBusUT.promise.complete(true);
        }
    }

    private static class ExampleChildListener  implements EventListener<ExampleEvent> {
        @Override
        public void handle(ExampleEvent event) {
            log.info("ExampleListener got event {}", event.toString());
            EventBusUT.childPromise.complete(true);
        }
    }

    @Before
    public void goBusGo() throws IDTakenException {

    }

    @Test
    public void testEventBus() throws ExecutionException, InterruptedException, IDTakenException {
        EventBus.MAIN.registerListener(new ExampleListener());
        ID randomBusID = EventBusID.create();
        EventBus.registerEventBus(randomBusID);
        EventBus.getByID(randomBusID).registerListener(new ExampleChildListener());
        EventBus.MAIN.fireEvent(new ExampleEvent());

        Assert.assertTrue(promise.get());
        Assert.assertTrue(childPromise.get());
        EventBus.MAIN.shutDownImmediate();

    }

}
