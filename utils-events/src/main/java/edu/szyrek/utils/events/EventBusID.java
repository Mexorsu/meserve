package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.IntegerID;
import edu.szyrek.utils.StringID;

/**
 * Created by bebebaba on 2016-08-14.
 */
public class EventBusID extends StringID {
    public static ID create(String val) throws IDTakenException {
        EventBusID res = new EventBusID();
        res.initWith(val);
        return res;
    }
    public static ID create() {
        EventBusID res = new EventBusID();
        res.initWithRandomString();
        return res;
    }
    public static ID get(String val) {
        EventBusID res = new EventBusID();
        res._idImpl = val;
        return res;
    }

    @Override
    public String toString() {
        return this._idImpl;
    }
}