package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by bebebaba on 2016-08-02.
 */
@Slf4j
public class EventBus implements Runnable {
    private static final int DEFAULT_EVENT_QUEUE_SIZE = 100;
    private static EventBusID MAIN_EVENT_BUS_ID;
    static {
        try {
            MAIN_EVENT_BUS_ID = (EventBusID) EventBusID.create("MAIN");
        } catch (IDTakenException e) {
            e.printStackTrace();
        }
    }
    private int eventQueueSize;
    private Set<EventListener> listeners = new HashSet<>();
    private boolean isRunning = true;
    private boolean isShutDown = false;
    private ID eventBusID = null;
    private EventBus parent = null;
    private Collection<EventBus> children = new HashSet<>();
    private Thread busThread;
    private Lock shutDownLock = new ReentrantLock();

    public static EventBus MAIN = MainEventBus.instance;

    private static class MainEventBus {
        public static EventBus instance;
        static {
            try {
                instance = registerEventBus(MAIN_EVENT_BUS_ID);
            } catch (IDTakenException e) {
                e.printStackTrace();
            }
            final Thread mainThread = Thread.currentThread();
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    if (!instance.isRunning || instance.isShutDown)  return;
                    instance.shutDown(false);
                    try {
                        mainThread.join();
                    } catch (InterruptedException e) {
                        log.error("Main thread interrupted", e);
                    }
                }
            });
        }
    }

    public static EventBus registerEventBus(ID id) throws IDTakenException {
        EventBus bus = new EventBus(id);
        if (id != MAIN_EVENT_BUS_ID) {
            MAIN.children.add(bus);
        }
        Thread thread = new Thread(bus);
        thread.setDaemon(true);
        bus.busThread = thread;
        thread.start();

        return bus;
    }

    private static EventBus getByIdInRoot(ID id, EventBus searchRoot) {
        if (searchRoot.eventBusID.equals(id)) {
            return searchRoot;
        } else if (searchRoot.hasChildren()){
            for (EventBus child: searchRoot.children) {
                EventBus potentialResult = getByIdInRoot(id, child);
                if (potentialResult!=null) {
                    return potentialResult;
                }
            }
            return null;
        } else {
            return null;
        }
    }

    public static EventBus getByID(ID id) {
        EventBus bus = getByIdInRoot(id, MAIN);
        MAIN.children.add(bus);
        return bus;
    }

    private EventBus() throws IDTakenException {
        this(DEFAULT_EVENT_QUEUE_SIZE);
    }

    private EventBus(EventBus parent) throws IDTakenException {
        this(DEFAULT_EVENT_QUEUE_SIZE, parent);
    }

    private EventBus(ID id) throws IDTakenException {
        this(DEFAULT_EVENT_QUEUE_SIZE, id, null);
    }

    private EventBus(ID id, EventBus parent) throws IDTakenException {
        this(DEFAULT_EVENT_QUEUE_SIZE, id, parent);
    }

    private EventBus(int size) throws IDTakenException {
        this(size, EventBusID.create(), null);
    }

    private EventBus(int size, EventBus parent) throws IDTakenException {
        this(size, EventBusID.create(), parent);
    }

    private EventBus(int size, ID busID, EventBus parent) throws IDTakenException {
        this.eventQueueSize = size;
        this.eventBusID = busID;
        this.parent = parent;
    }

    public void registerListener(EventListener listener) {
        this.listeners.add(listener);
    }

    public void ungeristerListener(EventListener listener) {
        this.listeners.add(listener);
    }


    private BlockingQueue<Event> eventsQueue = new LinkedBlockingQueue<>();

    public void fireEvent(Event e) {
        if (this.isShutDown) return;
        try {
            this.eventsQueue.put(e);
        } catch (InterruptedException e1) {
            log.error("Pushing Event {} interrupted", e);
        }
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    private void process(Event e) {
        log.trace("Processing {} @ EventBus: #{}#", e.toString(), eventBusID.toString());
        for (EventListener eventListener: listeners) {
            try {
                eventListener.handle(e);
                log.trace("Event {} handled @ EventBus: #{}#", e.toString(), eventBusID.toString());
                e.setProcessed(true);
                break;
            } catch (ClassCastException err) {
                continue;
            }
        }
        if (!e.isHandled()) {
            propagateToChildren(e);
        }
    }

    private void propagateToChildren(Event e) {
        for (EventBus bus: children){
            if (e.isHandled()) {
                return;
            } else {
                bus.process(e);
            }
        }
        if (eventBusID.equals(MAIN_EVENT_BUS_ID) && !e.isProcessed()) {
            log.warn("No listeners registered to handle event: {} ", e.toString());
        }
    }

    @Override
    public void run() {
        while (isRunning) {
            if (isShutDown) {
                shutDownMode();
            } else {
                buisinessAsUsual();
            }
        }
    }

    public void buisinessAsUsual() {
        try {
            process(eventsQueue.take());
        } catch (InterruptedException e) {
            log.error("EventBus interrupted", e);
        }
    }

    public void shutDownMode() {
        Event event = eventsQueue.poll();
        if (event != null) {
            process(event);
        } else {
            isRunning = false;
        }
    }


    private void cleanUp() {
        ID.release(eventBusID);
    }

    private void killChildrenFirst(boolean immediate) {
        for (EventBus tommy: children) {
            tommy.shutDown(immediate);
        }
    }

    public void shutDown(boolean immediate) {
        if (immediate) {
            shutDownImmediate();
        } else {
            shutDownGracefully();
        }
        cleanUp();
        tryNotify();
    }

    private void tryNotify(){
        try {
            if (!this.equals(EventBus.MAIN)) {
                shutDownLock.notify();
            }
        } catch (IllegalMonitorStateException e) {
            log.trace("Nobody cared about my shutdown {}",eventBusID);
        }
    }

    public Lock unlockWhenShutDown() {
        return shutDownLock;
    }

    public void waitForChildrenToDie() throws InterruptedException {
        for (EventBus child: children) {
            if (child.isRunning&&!child.isShutDown) {
                try {
                    child.unlockWhenShutDown().wait();
                } catch (IllegalMonitorStateException e) {
                    log.trace("Child already dead: {}", child.eventBusID);
                }
            }
        }
    }

    public void shutDownGracefully() {
        if (hasChildren()) {
            try {
                waitForChildrenToDie();
            } catch (InterruptedException e) {
                log.error("Interrupted while shutting down",e);
            }
        }
        this.isShutDown = true;
        eventsQueue.add(new EventBusShutDownEvent());
        try {
            this.busThread.join();
        } catch (InterruptedException e) {
            log.error("Interrupted while shutting down EventBus thread {}",eventBusID, e);
        }
        log.trace("EventBus {} shut down down gracefully", eventBusID);
    }

    public void shutDownImmediate() {
        killChildrenFirst(true);
        this.isShutDown = true;
        this.isRunning = false;
        try {
            this.busThread.join(100);
        } catch (InterruptedException e) {
            log.error("Interrupted while shutting down EventBus thread {}",eventBusID, e);
        }
        this.busThread.interrupt();
        log.warn("EventBus {} shut down in immediate mode", eventBusID);
    }

    public Set<EventListener> getListeners() {
        return listeners;
    }
}
