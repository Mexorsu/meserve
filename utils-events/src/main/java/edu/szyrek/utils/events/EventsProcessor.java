package edu.szyrek.utils.events;

import edu.szyrek.utils.annotations.EventListener;
import edu.szyrek.utils.meta.MetaAnnotationProcessor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * Created by bebebaba on 2016-08-02.
 */
@Slf4j
@SupportedAnnotationTypes({"edu.szyrek.utils.annotations.EventListener"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class EventsProcessor extends MetaAnnotationProcessor {
    private static JSONArray eventListeners = new JSONArray();

    static {
        eventListeners = metaConfig.containsKey(Events.EVENT_LISTENERS) ? (JSONArray) metaConfig.get(Events.EVENT_LISTENERS) : eventListeners;
    }


    @Override
    public boolean tryProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        try {
            for (TypeElement annotation : annotations) {
                for (Element el : roundEnv.getElementsAnnotatedWith(annotation)) {
                    if (EventListener.class.getSimpleName().equals(annotation.getSimpleName().toString())) {
                        EventListener eventListenerAnnotation = el.getAnnotation(EventListener.class);
                        String methodName = el.getSimpleName().toString();
                        String className = getFullName(el.getEnclosingElement());

                        JSONObject listener = new JSONObject();
                        listener.put("class", className);
                        listener.put("method", methodName);
                        eventListeners.add(listener);
                    }
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log.error("Failed to pre-register event listener in metaconfig", e);
        }
        metaConfig.put(Events.EVENT_LISTENERS, eventListeners);
        return true;
    }

    private static void registerListener(String className,  Element classElement) {
        String fullName = null;
        try {
            classElement.getClass().getDeclaredField("fullname").setAccessible(true);
            fullName = classElement.getClass().getDeclaredField("fullname").get(classElement).toString();
            classElement.getClass().getDeclaredField("fullname").setAccessible(false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        JSONObject mainClassObj = new JSONObject();
        mainClassObj.put("class", fullName == null ? classElement.getSimpleName().toString() : fullName);
        eventListeners.add(mainClassObj);
    }

}
