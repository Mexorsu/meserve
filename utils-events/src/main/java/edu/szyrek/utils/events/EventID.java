package edu.szyrek.utils.events;

import edu.szyrek.utils.ID;
import edu.szyrek.utils.IDTakenException;
import edu.szyrek.utils.IntegerID;

/**
 * Created by bebebaba on 2016-08-13.
 */
public class EventID extends IntegerID {
    public static ID create(int val) throws IDTakenException {
        EventID res = new EventID();
        res.initWith(val);
        return res;
    }
    public static ID create() {
        EventID res = new EventID();
        res.initWithRandomInteger();
        return res;
    }
}
