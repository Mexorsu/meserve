package edu.szyrek.utils.events.experimental;

/**
 * Created by bebebaba on 2016-08-02.
 */
public interface EventListener<Input,Output> {
    public Output handle(Input event);
}
