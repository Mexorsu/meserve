package edu.szyrek.utils.events;

/**
 * Created by bebebaba on 2016-08-01.
 */
public abstract class Event {
    private boolean processed = false;
    private boolean handled = false;
    private static long _id = 0;
    private static long getID() {
        return ++_id ;
    }
    private long id = getID();

    @Override
    public String toString() {
        return "Event of class "+this.getClass()+" with ID "+id;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }
}
