package edu.szyrek.utils.events;

/**
 * Created by bebebaba on 2016-08-02.
 */
public interface EventListener<EventType> {
    public void handle(EventType event);
}
