package com.example.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by bebebaba on 2016-07-30.
 */
public class GuessingGame {
    int lowerBound;
    int upperBound;
    int number;

    boolean guessed = false;

    public GuessingGame(int answer, int lowerBound, int upperBound) {
        if (lowerBound>=upperBound) {
            throw new IllegalArgumentException("Lower bound cannot be bigger than or equal to upper bound");
        }
        this.number = answer;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public GuessingGame(int lowerBound, int upperBound) {
        this(new Random(System.currentTimeMillis()).nextInt(upperBound-lowerBound)+lowerBound, lowerBound, upperBound);
    }


    public void start() {
        System.out.println("Guess a number between"+lowerBound+ " and "+upperBound);
        while (!guessed) {
            guessed = tryGuess();
        }
    }

    private boolean isInBounds(int answer) {
        return answer>=lowerBound && answer<=upperBound;
    }

    private boolean tryGuess() {
        int answer = -1;
        String input = null;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            input = reader.readLine();

            while ((answer = Integer.parseInt(input)) != number) {
                if (!isInBounds(answer)) {
                    System.out.println(answer+" is out of range of possible answers ("+lowerBound+","+upperBound+")");
                }
                if (answer < number) {
                    System.out.println(answer+" is too low, try again");
                    return false;
                } else {
                    System.out.println(answer + "is too high, try again");
                    return false;
                }
            }
            System.out.println("Right on, it's "+answer+", you guessed it! ;)");
            return true;
        } catch (NumberFormatException e) {
            System.err.println("Hey man, that's rude- "+input+" is not a number. Pick a number instead.");
            return false;
        } catch (IOException e) {
            System.err.print("Opps something broke... Input: "+input);
            return true;
        }
    }
}