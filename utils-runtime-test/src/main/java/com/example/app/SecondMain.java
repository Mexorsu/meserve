package com.example.app;

import edu.szyrek.utils.annotation.App;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bebebaba on 2016-07-31.
 */
@Slf4j
@App(name = "SecondApp", mainApp = true)
public class SecondMain {
    public static void main(String args[]) {
        System.out.println("Hello from utils-runtime-test");
    }
}
