package com.example.app;

import edu.szyrek.utils.annotation.App;
import edu.szyrek.utils.annotation.Configurable;
import edu.szyrek.utils.config.adapter.IntegerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by bebebaba on 2016-07-16.
 */
@Slf4j
@App(name="ExampleApp")
public class Main {
    @Configurable(key="lowerBound", adapter = IntegerAdapter.class)
    static Integer lower = 0;
    @Configurable(key="upperBound", adapter = IntegerAdapter.class)
    static Integer upper = 25;

    public static void main(String[] args) {
        GuessingGame game = new GuessingGame(lower,upper);
        System.out.println("Ok now let's play a game to test in/out stuff");
        game.start();
    }
}
